CC  = clang
STD = gnu11
AR = ar

SRC=src/
OBJ=obj/
DEP=dep/
TEST=test/
NAME=proj
LIB=aphys
LIBNAME=$(addprefix lib, $(addsuffix .a, $(LIB)))

UNITS = math/matherror math/vector math/matrix
ENTRY = main

TEST_UNITS = matherr vector matrix

WF=all extra error pedantic float-equal undef shadow pointer-arith cast-align \
 strict-overflow=3 write-strings aggregate-return cast-qual switch-default \
 switch-enum conversion unreachable-code format=2 error-implicit-function-declaration
LF=m
IF=$(SRC)
FF=trapv

WFLAGS = $(addprefix -W,$(WF))
LFLAGS = $(addprefix -l,$(LF))
IFLAGS = $(addprefix -I,$(IF))
FFLAGS = $(addprefix -f,$(FF))

OBJFLAG = -c -g $(IFLAGS) $(WFLAGS) $(FFLAGS)
BINFLAG = $(LIBNAME) $(LFLAGS)

TFLAGS = incompatible-pointer-types-discards-qualifiers sign-conversion shadow \
 float-equal shorten-64-to-32
TESTFLAGS = $(addprefix -Wno-,$(TFLAGS)) -DSNOW_ENABLED -I$(TEST) -I$(SRC)

OBJS = $(addprefix $(OBJ), $(addsuffix .o,$(UNITS)))
DEPS = $(addprefix $(DEP), $(addsuffix .d,$(UNITS)))
HDRS = $(shell find $(SRC) -name '*.h')
MAIN = $(addprefix $(OBJ), $(addsuffix .o, $(ENTRY)))
TESTS= $(addprefix $(TEST), $(addsuffix .t, $(TEST_UNITS)))

$(OBJ)%.o: $(SRC)%.c
	@mkdir -p $(dir $@)
	$(CC) -O3 -std=$(STD) $(OBJFLAG) $< -o $@

$(DEP)%.d: $(SRC)%.c $(HDRS)
	@mkdir -p $(dir $@)
	$(CC) -MM -MT $(patsubst $(SRC)%.c,$(OBJ)%.o,$<) $(OBJFLAG) -o $@ $<

include $(DEPS)

$(LIBNAME): $(OBJS)
	$(AR) rcs $@ $(OBJS)
	$(AR) -s $@

exec: $(LIBNAME) $(MAIN)
	$(CC) -static -std=$(STD) $(MAIN) $(BINFLAG) -o $(NAME)

clean:
	@rm -rf $(DEP)*
	@rm -rf $(OBJ)*
	@rm -f $(NAME)
	@rm -f vgcore*

clean-test:
	@rm -f $(TEST)*.t
	@rm -f vgcore*

clean-all: clean clean-test
	@rm -f $(LIBNAME)

t-clean: clean-test test

all: t-clean exec

$(TEST)%.t: $(TEST)%_test.c
	$(CC) $(OBJFLAG) -std=$(STD) $(TESTFLAGS) $< -o $(@:.t=.o)
	$(CC) -std=$(STD) -static $(@:.t=.o) $(BINFLAG) -o $@
	@rm $(@:.t=.o)

test: $(LIBNAME) $(TESTS)

#Run targets so i can use this file from atom
#run: clean
#run: clean-test
#run: clean clean-test
#run: all
#run: test
#run: exec

.PHONY : all clean test clean-test exec t-clean
.DEFAULT_GOAL := exec
