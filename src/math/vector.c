#include "math/vector.h"

#include <stdlib.h>
#include <stdbool.h>
#include <memory.h>
#include <math.h>

MATH_ERROR vec_init(struct math_vec *v, size_t l)
{
  if (!v)
    return MATH_INVARG;

  if (l == 0)
    return MATH_INVARG;

  v->degree = l;
  v->data = malloc(sizeof(double) * l);
  memset(v->data, 0, sizeof(double) * l);

  return MATH_NOERR;
}

MATH_ERROR vec_free(struct math_vec *v)
{
  if (!v)
    return MATH_INVARG;

  v->degree = 0;
  free(v->data);
  v->data = NULL;

  return MATH_NOERR;
}

MATH_ERROR vec_mult(struct math_vec *v, double s)
{
  if (!v || !v->data)
    return MATH_INVARG;

  for (size_t i=0; i<v->degree; i++)
    v->data[i] *= s;

  return MATH_NOERR;
}

MATH_ERROR vec_add(struct math_vec *v, const struct math_vec *w)
{
  if (!v || !w)
    return MATH_INVARG;

  if (!v->data || !w->data)
    return MATH_INVARG;

  if (v->degree != w->degree)
    return MATH_DIMENSION;

  for (size_t i=0; i<v->degree; i++)
    v->data[i] += w->data[i];

  return MATH_NOERR;
}

MATH_ERROR vec_sub(struct math_vec *v, const struct math_vec *w)
{
  if (!v || !w)
    return MATH_INVARG;

  if (!v->data || !w->data)
    return MATH_INVARG;

  if (v->degree != w->degree)
    return MATH_DIMENSION;

  for (size_t i=0; i<v->degree; i++)
    v->data[i] -= w->data[i];

  return MATH_NOERR;
}

MATH_ERROR vec_dot(double *ret, const struct math_vec *v, const struct math_vec *w)
{
  if (!ret || !v || !w)
    return MATH_INVARG;

  if (!v->data || !w->data)
    return MATH_INVARG;

  if (v->degree != w->degree)
    return MATH_DIMENSION;

  *ret = 0;

  for (size_t i=0; i<v->degree; i++)
    *ret += v->data[i] * w->data[i];

  return MATH_NOERR;
}

MATH_ERROR vec_ang(double *ret, const struct math_vec *v, const struct math_vec *w)
{
  if (!v || !w || !ret)
    return MATH_INVARG;
  if (!v->data || !w->data)
    return MATH_INVARG;

  double dot = 0.0;

  MATH_ERROR e = vec_dot(&dot, v, w);
  if (e != MATH_NOERR)
    return e;

  double vl = 0, wl = 0;
  e = vec_mod(&vl, v);
  if (e != MATH_NOERR)
    return MATH_INTERN;

  e = vec_mod(&wl, w);
  if (e != MATH_NOERR)
    return MATH_INTERN;

  if (fabs(vl) < EPSILON || fabs(wl) < EPSILON)
    return MATH_DIVZ;

  *ret = acos(dot / (vl * wl));

  return MATH_NOERR;
}

MATH_ERROR vec_mod(double *ret, const struct math_vec *v)
{
  if (!ret || !v)
    return MATH_INVARG;
  if (!v->data)
    return MATH_INVARG;

  *ret = 0.0;
  for (size_t i=0; i<v->degree; i++)
    *ret += pow(v->data[i], 2.0);

  *ret = sqrt(*ret);

  return MATH_NOERR;
}

MATH_ERROR vec_cross(struct math_vec *v, const struct math_vec *w)
{
  if (!v || !w || !v->data || !w->data)
    return MATH_INVARG;

  if (v->degree != w->degree || v->degree != 3)
    return MATH_DIMENSION;

  //a x b = (aybz - azby)i + (azbx - axbz)j + (axby - aybx)k
  double i = v->data[1]*w->data[2] - v->data[2]*w->data[1];
  double j = v->data[2]*w->data[0] - v->data[0]*w->data[2];
  double k = v->data[0]*w->data[1] - v->data[1]*w->data[0];

  v->data[0] = i;
  v->data[1] = j;
  v->data[2] = k;

  return MATH_NOERR;
}

MATH_ERROR vec_norm(struct math_vec *v)
{
  if (!v || !v->data)
    return MATH_INVARG;

  double len = 0.0;
  vec_mod(&len, v);

  if (fabs(len) < EPSILON)
    return MATH_DIVZ;

  for (size_t i=0; i<v->degree; i++)
    v->data[i] /= len;

  return MATH_NOERR;
}

MATH_ERROR vec_eq(bool *b, const struct math_vec *v, const struct math_vec *w)
{
  if (!b || !v || !w || !v->data || !w->data)
    return MATH_INVARG;

  if (v->degree != w->degree)
    return MATH_DIMENSION;

  *b = true;

  for (size_t i=0; i<v->degree; i++)
    if (fabs(v->data[i] - w->data[i]) > EPSILON)
      *b = false;

  return MATH_NOERR;
}

MATH_ERROR vec_cp(struct math_vec *v, const struct math_vec *w)
{
  if (!v || !w || !v->data || !w->data)
    return MATH_INVARG;

  if (v->degree != w->degree)
    return MATH_DIMENSION;

  memcpy(v->data, w->data, sizeof(double) * v->degree);

  return MATH_NOERR;
}
