#ifndef MATHERROR_H__
#define MATHERROR_H__

#include <stdio.h>
#include <stdint.h>

#ifndef PI
#define PI 3.141592653589793238462643383279502884197169399375105820974
#endif

#ifndef EPSILON
#define EPSILON 0.000000001
#endif

typedef enum {
  MATH_NOERR,
  MATH_SHRTBUF,
  MATH_INVARG,
  MATH_INTERN,
  MATH_DIMENSION,
  MATH_DIVZ,
  MATH_OOB
} MATH_ERROR;

//Write error code string to stderr,
MATH_ERROR math_perror(MATH_ERROR);
//Write error code string to FILE
MATH_ERROR math_perrorf(MATH_ERROR, FILE *);
//Will return MATH_SHRTBUF if the char array is too small,
//MATH_INVARG if either char * is NULL or size_t is 0,
//otherwise MATH_NOERR
MATH_ERROR math_errorstring(MATH_ERROR, char *, size_t);

#endif
