#include "matherror.h"
#include <stdio.h>
#include <string.h>

MATH_ERROR math_perror(MATH_ERROR m)
{
  MATH_ERROR e = math_perrorf(m, stderr);
  return e;
}

MATH_ERROR math_perrorf(MATH_ERROR m, FILE *f)
{
  if (f == NULL)
    return MATH_INVARG;

  char str[32];
  MATH_ERROR e = math_errorstring(m, str, sizeof(str));

  if (e == MATH_NOERR) {
    fprintf(f, "%s\n", str);
    return e;
  }

  return MATH_INTERN;
}

MATH_ERROR math_errorstring(MATH_ERROR m, char *str, size_t l)
{
  if (str == NULL || l < 1)
    return MATH_INVARG;

  const char *list[] = {
    "Math: No Error",
    "Math: Short Buffer",
    "Math: Invalid Argument",
    "Math: Internal Function Error",
    "Math: Dimension Mismatch",
    "Math: Division by Zero",
    "Math: Out of Bounds"
  };


  if (m > MATH_OOB)
    return MATH_OOB;

  size_t llen = strlen(list[m]) + 1;

  if (l < llen)
    return MATH_SHRTBUF;

  strncpy(str, list[m], llen);

  return MATH_NOERR;
}
