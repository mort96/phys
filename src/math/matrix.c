#include "math/matrix.h"

#include "stdio.h"
#include <stdlib.h>
#include <memory.h>
#include <math.h>
#include <stdio.h>

__attribute__((unused)) static void mat_print(const struct math_mat *m, FILE* f)
{
  if (!m || !f || !m->data)
    return;

  fprintf(f, "[");
  for (size_t i=0; i<m->row; i++) {
    fprintf(f, "[");
    for(size_t j=0; j<m->col; j++)
      fprintf(f, " %f ", m->data[i][j]);
    fprintf(f, "]\n ");
  }
  fprintf(f, "]\n");
}

MATH_ERROR mat_init(struct math_mat *m, size_t r, size_t c)
{
  if (!m)
    return MATH_INVARG;
  if (r == 0 || c == 0)
    return MATH_INVARG;

  m->row = r;
  m->col = c;

  m->data = malloc(r * sizeof(double *));

  for (size_t i=0; i<r; i++) {
    m->data[i] = malloc(c * sizeof(double));
    memset(m->data[i], 0, c*sizeof(double));
  }

  return MATH_NOERR;
}

MATH_ERROR mat_free(struct math_mat *m)
{
  if (!m || !m->data)
    return MATH_INVARG;

  for (size_t i=0; i<m->row; i++)
    free(m->data[i]);
  free(m->data);
  m->data = NULL;
  m->row = 0;
  m->col = 0;

  return MATH_NOERR;
}

MATH_ERROR mat_scale(struct math_mat *m, double s)
{
  if (!m || !m->data)
    return MATH_INVARG;

  for (size_t i=0; i<m->row; i++)
    for (size_t j=0; j<m->col; j++)
      m->data[i][j] *= s;

  return MATH_NOERR;
}

MATH_ERROR mat_add(struct math_mat *m, const struct math_mat *n)
{
  if (!m || !n || !m->data || !n->data)
    return MATH_INVARG;

  if (m->row != n->row || m->col != n->col)
    return MATH_DIMENSION;

  for (size_t i=0; i<m->row; i++)
    for (size_t j=0; j<m->col; j++)
      m->data[i][j] += n->data[i][j];

  return MATH_NOERR;
}

MATH_ERROR mat_sub(struct math_mat *m, const struct math_mat *n)
{
  if (!m || !n || !m->data || !n->data)
    return MATH_INVARG;

  if(m->row != n->row || m->col != n->col)
    return MATH_DIMENSION;

  for (size_t i=0; i<m->row; i++)
    for (size_t j=0; j<m->col; j++)
      m->data[i][j] -= n->data[i][j];

  return MATH_NOERR;
}

MATH_ERROR mat_pow(struct math_mat *m, int p)
{
  if (!m || !m->data)
    return MATH_INVARG;

  if (m->col != m->row)
    return MATH_DIMENSION;

  for (size_t i=0; i<m->row; i++)
    for (size_t j=0; j<m->col; j++)
      if (i == j)
        continue;
      else if (fabs(m->data[i][j]) > EPSILON)
        return MATH_INVARG;

  for (size_t i=0; i<m->row; i++)
    m->data[i][i] = pow(m->data[i][i], p);

  return MATH_NOERR;
}

MATH_ERROR mat_ident(struct math_mat *m)
{
  if (!m || !m->data)
    return MATH_INVARG;

  if (m->row != m->col)
    return MATH_DIMENSION;

  for (size_t i=0; i<m->row; i++)
    for (size_t j=0; j<m->col; j++)
      if (i == j)
        m->data[i][j] = 1.0;
      else
        m->data[i][j] = 0.0;

  return MATH_NOERR;
}

MATH_ERROR mat_rowvec(struct math_vec *v, const struct math_mat *m, size_t ind)
{
  if (!v || !m || !v->data || !m->data)
    return MATH_INVARG;
  if (v->degree != m->col)
    return MATH_DIMENSION;
  if (ind >= m->row)
    return MATH_OOB;

  for (size_t i=0; i<m->col; i++)
    v->data[i] = m->data[ind][i];

  return MATH_NOERR;
}

MATH_ERROR mat_colvec(struct math_vec *v, const struct math_mat *m, size_t ind)
{
  if (!v || !m || !v->data || !m->data)
    return MATH_INVARG;
  if (v->degree != m->row)
    return MATH_DIMENSION;
  if (ind >= m->col)
    return MATH_OOB;

  for (size_t i=0; i<m->row; i++)
    v->data[i] = m->data[i][ind];

  return MATH_NOERR;
}

MATH_ERROR mat_mult(struct math_mat *m, const struct math_mat *n)
{
  if (!m || !n || !m->data || !n->data)
    return MATH_INVARG;

  if (m->col != n->row)
    return MATH_DIMENSION;

  double d = 0.0;

  struct math_mat tmp = {0, 0, NULL};
  mat_init(&tmp, m->row, n->col);

  struct math_vec v = {0, NULL};
  struct math_vec w = {0, NULL};
  vec_init(&v, m->col);
  vec_init(&w, n->row);

  for (size_t i=0; i<tmp.row; i++)
    for (size_t j=0; j<tmp.col; j++) {
      mat_rowvec(&v, m, i);
      mat_colvec(&w, n, j);
      vec_dot(&d, &v, &w);
      tmp.data[i][j] = d;
    }

  vec_free(&v);
  vec_free(&w);

  mat_free(m);
  mat_init(m, tmp.row, tmp.col);

  mat_cp(m, &tmp);

  mat_free(&tmp);

  return MATH_NOERR;
}

MATH_ERROR mat_operate(struct math_vec *v, const struct math_mat *m)
{
  if (!v || !m || !v->data || !m->data)
    return MATH_INVARG;

  if (v->degree != m->col)
    return MATH_DIMENSION;

  struct math_vec w = {0, NULL};
  struct math_vec tmp = {0, NULL};
  vec_init(&w, m->col);
  vec_init(&tmp, m->row);

  double d = 0.0;

  for (size_t i=0; i<m->row; i++) {
    mat_rowvec(&w, m, i);
    vec_dot(&d, &w, v);
    tmp.data[i] = d;
  }

  vec_free(&w);

  vec_free(v);
  vec_init(v, tmp.degree);

  vec_cp(v, &tmp);

  vec_free(&tmp);
  return MATH_NOERR;
}

MATH_ERROR mat_vtocm(struct math_mat *m, const struct math_vec *v)
{
  if (!m || !v || !m->data || !v->data)
    return MATH_INVARG;

  if (m->col != 1 || m->row != v->degree)
    return MATH_DIMENSION;

  for (size_t i=0; i<v->degree; i++)
    m->data[i][0] = v->data[i];

  return MATH_NOERR;
}

MATH_ERROR mat_vtorm(struct math_mat *m, const struct math_vec *v)
{
  if (!m || !v || !m->data || !v->data)
    return MATH_INVARG;

  if (m->row != 1 || m->col != v->degree)
    return MATH_DIMENSION;

  for (size_t i=0; i<v->degree; i++)
    m->data[0][i] = v->data[i];

  return MATH_NOERR;
}

MATH_ERROR mat_setrow(struct math_mat *m, const struct math_vec *v, size_t ind)
{
  if (!m || !m->data || !v || !v->data)
    return MATH_INVARG;
  if (m->col != v->degree)
    return MATH_DIMENSION;
  if (ind >= m->row)
    return MATH_OOB;

  for (size_t i=0; i<v->degree; i++)
    m->data[ind][i] = v->data[i];

  return MATH_NOERR;
}

MATH_ERROR mat_setcol(struct math_mat *m, const struct math_vec *v, size_t ind)
{
  if (!m || !m->data || !v || !v->data)
    return MATH_INVARG;
  if (m->row != v->degree)
    return MATH_DIMENSION;
  if (ind >= m->col)
    return MATH_OOB;

  for (size_t i=0; i<v->degree; i++)
    m->data[i][ind] = v->data[i];

  return MATH_NOERR;
}

MATH_ERROR mat_transpose(struct math_mat *m) {
  if (!m || !m->data)
    return MATH_INVARG;

  struct math_mat tmp = {0, 0, NULL};
  mat_init(&tmp, m->col, m->row);

  struct math_vec v = {0, NULL};
  vec_init(&v, m->col);

  for (size_t i=0; i<m->row; i++) {
    mat_rowvec(&v, m, i);
    mat_setcol(&tmp, &v, i);
  }

  vec_free(&v);

  mat_free(m);
  mat_init(m, tmp.row, tmp.col);

  mat_cp(m, &tmp);

  mat_free(&tmp);
  return MATH_NOERR;
}

MATH_ERROR mat_det(double *ret, const struct math_mat *m)
{
  if (!m || !ret || !m->data)
    return MATH_INVARG;

  if (m->row != m->col)
    return MATH_DIMENSION;

  //Special cases of determinate
  if (m->row == 0) {
    *ret = 1.0;
    return MATH_NOERR;
  }
  if (m->row == 1) {
    *ret = m->data[0][0];
    return MATH_NOERR;
  }
  if (m->row == 2) {
    *ret = m->data[0][0]*m->data[1][1] - m->data[1][0]*m->data[0][1];
    return MATH_NOERR;
  }
  if (m->row == 3) {
    //a b c
    //d e f
    //g h i
    //det = aei + bfg + cdh - gec - hfa - idb
    *ret  = m->data[0][0] * m->data[1][1] * m->data[2][2]; //aei
    *ret += m->data[0][1] * m->data[1][2] * m->data[2][0]; //bfg
    *ret += m->data[0][2] * m->data[1][0] * m->data[2][1]; //cdh
    *ret -= m->data[2][0] * m->data[1][1] * m->data[0][2]; //-gec
    *ret -= m->data[2][1] * m->data[1][2] * m->data[0][0]; //-hfa
    *ret -= m->data[2][2] * m->data[1][0] * m->data[0][1]; //-idb
    return MATH_NOERR;
  }

  struct math_mat n = {0, 0, NULL};
  mat_init(&n, m->row, m->col);

  mat_cp(&n, m);

  //If we turn the matrix into a triangle matrix then det(m) = prod(diagnal elems)
  //row echelon form is triangular
  double f = 0.0;
  mat_ref(&n, &f);

  *ret = f;
  for (size_t i=0; i<m->row; i++)
    *ret *= n.data[i][i];

  mat_free(&n);
  return MATH_NOERR;
}

MATH_ERROR mat_inv(struct math_mat *m)
{
  if (!m || !m->data)
    return MATH_INVARG;
  if (m->row != m->col)
    return MATH_DIMENSION;

  double d = 0.0;
  mat_det(&d, m);

  if (d == 0.0)
    return MATH_DIVZ;

  //Special cases
  if (m->row == 0)
    return MATH_NOERR;
  if (m->row == 1) {
    m->data[0][0] = 1/m->data[0][0];
    return MATH_NOERR;
  }
  if (m->row == 2) {
    m->data[0][1] *= -1;
    m->data[1][0] *= -1;
    double tmp = m->data[0][0];
    m->data[0][0] = m->data[1][1];
    m->data[1][1] = tmp;
    mat_scale(m, 1/d);
    return MATH_NOERR;
  }

  //Make an augmented matrix, perform g-j elimination, take the right part
  struct math_mat n = {0, 0, NULL};
  mat_init(&n, m->row, m->col*2);

  for (size_t i=0; i<m->row; i++) {
    for (size_t j=0; j<m->col; j++) {
      n.data[i][j] = m->data[i][j];
      if (i == j)
        n.data[i][j + m->col] = 1;
      else
        n.data[i][j + m->col] = 0;
    }
  }

  mat_rref(&n, NULL);
  mat_submatrix2(m, &n, n.col / 2, n.col - 1, 0, n.row - 1);

  mat_free(&n);
  return MATH_NOERR;
}

MATH_ERROR mat_rowop(struct math_mat *m, size_t a, size_t b, double x, double y)
{
  if (!m || !m->data)
    return MATH_INVARG;

  if (a >= m->row || b >= m->row)
    return MATH_OOB;

  struct math_vec v = {0, NULL};
  struct math_vec w = {0, NULL};
  vec_init(&v, m->col);
  vec_init(&w, m->col);

  mat_rowvec(&v, m, a);
  mat_rowvec(&w, m, b);

  vec_mult(&v, x);
  vec_mult(&w, y);

  vec_add(&v, &w);

  mat_setrow(m, &v, a);

  vec_free(&v);
  vec_free(&w);
  x++;
  y++;
  return MATH_NOERR;
}

MATH_ERROR mat_rowswap(struct math_mat *m, size_t a, size_t b)
{
  if (!m || !m->data)
    return MATH_INVARG;

  if (a >= m->row || b >= m->row)
    return MATH_OOB;

  struct math_vec v = {0, NULL};
  struct math_vec w = {0, NULL};
  vec_init(&v, m->col);
  vec_init(&w, m->col);

  mat_rowvec(&v, m, a);
  mat_rowvec(&w, m, b);

  mat_setrow(m, &v, b);
  mat_setrow(m, &w, a);

  vec_free(&v);
  vec_free(&w);
  return MATH_NOERR;
}

MATH_ERROR mat_ref(struct math_mat *m, double *det)
{
  if (!m || !m->data)
    return MATH_INVARG;

  double det_fake = 0.0;
  if (!det)
    det = &det_fake;

  *det = 1.0;

  size_t y = 0, x = 0;

  //ref algorithm
  while (y < m->row && x < m->col) {
    //If the current pivot is 0, try to find a lower row that has a non-zero pivot at this x
    if (fabs(m->data[y][x]) < EPSILON) {
      size_t ind = 0;
      for (ind=y; ind<m->row; ind++) {
        //activates on first row under y to have a non-zero element in the column
        if (fabs(m->data[ind][x]) < EPSILON) {
          *det *= -1;
          mat_rowswap(m, y, ind);
          break;
        }
      }

      //If not found, increment x and redo the loop
      if (ind == m->row) {
        ++x;
        continue;
      }
    }

    //Now that we have our pivot, make all other entries in the x column 0
    for (size_t ind = 0; ind<m->row; ind++) {
      if (ind == y)
        continue;

      //If the value is already 0 we can move on
      if (fabs(m->data[ind][x]) < EPSILON)
        continue;

      //Factor so that [index][x] = [y][x]*f,
      //So [index][x] + -f[y][x] = 0
      double f = m->data[ind][x] / m->data[y][x];
      mat_rowop(m, ind, y, 1.0, -f);
    }

    ++x; ++y;
  }

  return MATH_NOERR;
}

MATH_ERROR mat_rref(struct math_mat *m, double *det)
{
  if (!m || !m->data)
    return MATH_INVARG;

  double det_fake = 0.0;
  if (!det)
    det = &det_fake;

  mat_ref(m, det);

  struct math_vec v = {0, NULL};
  vec_init(&v, m->col);

  //Reduce the first non-zero element of every row to 1
  for (size_t i=0; i<m->row; i++) {
    mat_rowvec(&v, m, i);
    size_t j = 0;
    while (j < v.degree && fabs(v.data[j]) < EPSILON)
      j++;

    if (j == v.degree)
      continue;

    double f = v.data[j];
    *det /= f;
    vec_mult(&v, 1/f);
    mat_setrow(m, &v, i);
  }

  vec_free(&v);
  return MATH_NOERR;
}

MATH_ERROR mat_submatrix(
 struct math_mat *m,
 const struct math_mat *n,
 size_t a,
 size_t b)
{
  if (!m || !n || !m->data || !n->data)
    return MATH_INVARG;
  if (m->row + 1 != n->row || m->col + 1 != n->col)
    return MATH_DIMENSION;
  if (a >= n->row || b >= n->col)
    return MATH_OOB;

  size_t x = 0, y = 0;
  for (size_t i=0; i<n->row; i++) {
    if (i == a)
      continue;

    x = 0;
    for (size_t j=0; j<n->col; j++) {
      if (j == b)
        continue;
      m->data[y][x] = n->data[i][j];
      ++x;
    }
    ++y;
  }

  return MATH_NOERR;
}

MATH_ERROR mat_submatrix2(
  struct math_mat *m,
  const struct math_mat *n,
  size_t left,
  size_t right,
  size_t top,
  size_t bottom)
{
  if (!m || !n || !m->data || !n->data)
    return MATH_INVARG;

  size_t tmp;
  if (left > right) {
    tmp = left;
    left = right;
    right = tmp;
  }

  if (top > bottom) {
    tmp = top;
    top = bottom;
    bottom = tmp;
  }

  if (bottom >= n->row || right >= n->col)
    return MATH_OOB;

  if (m->row != bottom - top + 1 || m->col != right - left + 1)
    return MATH_DIMENSION;

  size_t y = 0, x = 0;
  for (size_t i=0; i<n->row; i++) {
    if (i < top || i > bottom)
      continue;

    x = 0;
    for (size_t j=0; j<n->col; j++) {
      if (j < left || j > right)
        continue;

      m->data[y][x] = n->data[i][j];
      x++;
    }
    y++;
  }

  return MATH_NOERR;
}

MATH_ERROR mat_minor(struct math_mat *m)
{
  if (!m || !m->data)
    return MATH_INVARG;
  if (m->col != m->row)
    return MATH_DIMENSION;

  struct math_mat n = {0, 0, NULL};

  //Special cases
  if (m->row <= 1)
    return MATH_NOERR;
  if (m->row == 2) {
    mat_init(&n, 2, 2);
    n.data[0][0] = m->data[1][1]; n.data[0][1] = m->data[1][0];
    n.data[1][0] = m->data[0][1]; n.data[1][1] = m->data[0][0];

    mat_cp(m, &n);
    mat_free(&n);
    return MATH_NOERR;
  }

  mat_init(&n, m->row, m->col);

  struct math_mat sub = {0, 0, NULL};
  mat_init(&sub, m->row - 1, m->col - 1);

  double d = 0.0;
  for (size_t i=0; i<m->row; i++) {
    for (size_t j=0; j<m->col; j++) {
      mat_submatrix(&sub, m, i, j);
      mat_det(&d, &sub);
      n.data[i][j] = d;
    }
  }

  mat_cp(m, &n);

  mat_free(&n);
  mat_free(&sub);

  return MATH_NOERR;
}

MATH_ERROR mat_adj(struct math_mat *m)
{
  if (!m || !m->data)
    return MATH_INVARG;
  if (m->row != m->col)
    return MATH_DIMENSION;

  mat_transpose(m);
  mat_minor(m);

  for (size_t i=0; i<m->row; i++)
    for (size_t j=0; j<m->col; j++)
      if ((i+j) & 1)
      m->data[i][j] *= -1;

  return MATH_NOERR;
}

MATH_ERROR mat_eq(bool *b, const struct math_mat *m, const struct math_mat *n)
{
  if (!b || !m || !n || !m->data || !n->data)
    return MATH_INVARG;

  if (m->row != n->row || m->col != n->col)
    return MATH_DIMENSION;

  *b = true;

  for (size_t i=0; i<m->row; i++)
    for (size_t j=0; j<m->col; j++)
      if (fabs(m->data[i][j] - n->data[i][j]) > EPSILON) {
        *b = false;
        return MATH_NOERR;
      }

  return MATH_NOERR;
}

MATH_ERROR mat_cp(struct math_mat *m, const struct math_mat *n)
{
  if (!m || !n || !m->data || !n->data)
    return MATH_INVARG;
  if (m->col != n->col || m->row != n->row)
    return MATH_DIMENSION;

  size_t len = sizeof(double) * n->col;
  for (size_t i=0; i<n->row; i++)
    memcpy(m->data[i], n->data[i], len);

  return MATH_NOERR;
}
