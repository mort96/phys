#ifndef VECTOR_H__
#define VECTOR_H__

#include <stdint.h>
#include <stdbool.h>
#include "math/matherror.h"

struct math_vec {
  size_t degree;
  double *data;
};

//Sets up the struct. Memory must be allocated before
MATH_ERROR vec_init(struct math_vec *, size_t);
//Free the data of the vector. The vector pointer itself must be freed manually
MATH_ERROR vec_free(struct math_vec *);
MATH_ERROR vec_mult(struct math_vec *, double);
//All of the arithmatic operations place the answer into the first operand
MATH_ERROR vec_add(struct math_vec *, const struct math_vec *);
MATH_ERROR vec_sub(struct math_vec *, const struct math_vec *);
MATH_ERROR vec_dot(double *, const struct math_vec *, const struct math_vec *);
//Returns the smaller of the two values, in range [0, PI]
MATH_ERROR vec_ang(double *, const struct math_vec *, const struct math_vec *);
MATH_ERROR vec_mod(double *, const struct math_vec *);
//Gives a MATH_DIMENSION error on any non-3D vector
MATH_ERROR vec_cross(struct math_vec *, const struct math_vec *);
//Normalize the vector
MATH_ERROR vec_norm(struct math_vec *);

MATH_ERROR vec_eq(bool *, const struct math_vec *, const struct math_vec *);
MATH_ERROR vec_cp(struct math_vec *, const struct math_vec *);

#endif
