#ifndef MATRIX_H__
#define MATRIX_H__

#include <stdlib.h>
#include <stdbool.h>
#include "math/vector.h"

struct math_mat {
  size_t row, col;
  double **data;
};

MATH_ERROR mat_init(struct math_mat *, size_t, size_t);
MATH_ERROR mat_free(struct math_mat *);
MATH_ERROR mat_scale(struct math_mat *, double);
MATH_ERROR mat_add(struct math_mat *, const struct math_mat *);
MATH_ERROR mat_sub(struct math_mat *, const struct math_mat *);
//Only usable on diagnal matricies, otherwise throw MATH_INVARG
MATH_ERROR mat_pow(struct math_mat *, int);
//Only usable on square matricies, otherwise throw MATH_DIMENSION
MATH_ERROR mat_ident(struct math_mat *);
MATH_ERROR mat_rowvec(struct math_vec *, const struct math_mat *, size_t);
MATH_ERROR mat_colvec(struct math_vec *, const struct math_mat *, size_t);
//Dimensions of first matrix change if needed (after the multiplication)
MATH_ERROR mat_mult(struct math_mat *, const struct math_mat *);
//Multiply by a vector (matrix * vector), all vectors are column vectors
//To do (vector * matrix), change the vector into a matrix
MATH_ERROR mat_operate(struct math_vec *, const struct math_mat *);
MATH_ERROR mat_vtocm(struct math_mat *, const struct math_vec *);
MATH_ERROR mat_vtorm(struct math_mat *, const struct math_vec *);
MATH_ERROR mat_setrow(struct math_mat *, const struct math_vec *, size_t);
MATH_ERROR mat_setcol(struct math_mat *, const struct math_vec *, size_t);
MATH_ERROR mat_transpose(struct math_mat *);
MATH_ERROR mat_det(double *, const struct math_mat *);
//Throws MATH_DIMENSION on non square matricies, MATH_DIVZ on noninvertible matricies
MATH_ERROR mat_inv(struct math_mat *);
//First size_t is INDA, second is INDB, the doubles are (A, B)
//modifies the matrix so that row[INDA] = Arow[INDA] + Brow[INDB]
MATH_ERROR mat_rowop(struct math_mat *, size_t, size_t, double, double);
MATH_ERROR mat_rowswap(struct math_mat *, size_t, size_t);
//The double pointers are optional arguments for returning the effect of the operation
//On the determinate
MATH_ERROR mat_ref(struct math_mat *, double *);
MATH_ERROR mat_rref(struct math_mat *, double *);
//The first submatrix excludes the row and col, submatrix2 specifies a box to keep
//The limits on submatrix2 are inclusive
MATH_ERROR mat_submatrix(struct math_mat *, const struct math_mat *, size_t, size_t);
MATH_ERROR mat_submatrix2(struct math_mat *, const struct math_mat *, size_t, size_t, size_t, size_t);
MATH_ERROR mat_minor(struct math_mat *);
MATH_ERROR mat_adj(struct math_mat *);


//TODO After i write a polynomial solver
//the vec array and double array must both be at least the size of the matrix
//A non-square matrix throws MATH_DIMENSION
//Invalid arrays throw MATH_INVARG
MATH_ERROR mat_eigen(struct math_vec **, double **, const struct math_mat *);

MATH_ERROR mat_eq(bool *, const struct math_mat *, const struct math_mat *);
MATH_ERROR mat_cp(struct math_mat *, const struct math_mat *);

#endif
