#include "math/matrix.h"
#include "math/vector.h"

#include <snow.h>

describe(matrix, {
  struct math_mat *m, *n;
  m = malloc(sizeof(struct math_mat));
  n = malloc(sizeof(struct math_mat));
  __attribute__((unused)) MATH_ERROR e;

  subdesc(mat_init, {
    it("Catches NULL pointers", {
      e = mat_init(NULL, 1, 1);
      asserteq(e, MATH_INVARG);
    });

    it("Catches 0 dimensions", {
      e = mat_init(m, 0, 1);
      asserteq(e, MATH_INVARG);
      e = mat_init(m, 1, 0);
      asserteq(e, MATH_INVARG);
    });

    it("Correctly initializes", {
      e = mat_init(m, 2, 3);
      asserteq(e, MATH_NOERR);
      asserteq(m->row, 2);
      asserteq(m->col, 3);
      assertneq(m->data, NULL);

      for (size_t i=0; i<m->row; i++)
        assertneq(m->data[i], NULL);

      for (size_t i=0; i<n->row; i++)
        for (size_t j=0; j<m->col; j++)
          asserteq(m->data[i][j], 0.0);
    });
  });

  subdesc(mat_free, {
    it("Catches NULL pointers", {
      e = mat_free(NULL);
      asserteq(e, MATH_INVARG);
    });

    it("Frees a matrix", {
      e = mat_free(m);
      asserteq(e, MATH_NOERR);
      asserteq(m->row, 0);
      asserteq(m->col, 0);
      asserteq(m->data, NULL);
    });

    it("Catches an already free'd matrix", {
      e = mat_free(m);
      asserteq(e, MATH_INVARG);
    });
  });

  subdesc(mat_scale, {
    it("Catches NULL pointers", {
      e = mat_scale(NULL, 0);
      asserteq(e, MATH_INVARG);
    });

    it("Catches already free'd matricies", {
      e = mat_scale(m, 0);
      asserteq(e, MATH_INVARG);
    });

    it("Scales a matrix", {
      mat_init(m, 2, 2);

      m->data[0][0] = 2; m->data[0][1] = 3;
      m->data[1][0] = 3; m->data[1][1] = 2;

      defer(mat_free(m););

      e = mat_scale(m, 2);
      asserteq(e, MATH_NOERR);
      asserteq(m->data[0][0], 4.0); asserteq(m->data[0][1], 6.0);
      asserteq(m->data[1][0], 6.0); asserteq(m->data[1][1], 4.0);
    });
  });

  subdesc(mat_add, {
    mat_init(m, 2, 3);

    it("Catches NULL pointers", {
      e = mat_add(NULL, NULL);
      asserteq(e, MATH_INVARG);

      e = mat_add(m, NULL);
      asserteq(e, MATH_INVARG);

      e = mat_add(NULL, m);
      asserteq(e, MATH_INVARG);
    });

    mat_free(m);
    mat_init(n, 2, 2);

    it("Catches already free'd matricies", {
      e = mat_add(m, n);
      asserteq(e, MATH_INVARG);
      e = mat_add(n, m);
      asserteq(e, MATH_INVARG);
    });

    mat_init(m, 2, 3);

    it("Catches dimension mismatches", {
      e = mat_add(m, n);
      asserteq(e, MATH_DIMENSION);
      e = mat_add(n, m);
      asserteq(e, MATH_DIMENSION);
    });

    mat_free(n);
    mat_init(n, 2, 3);

    it("Adds matricies", {
      n->data[0][0] = 1; n->data[0][1] = -5; n->data[0][2] = 2;
      n->data[1][0] = 3; n->data[1][1] =  6; n->data[1][2] = 2;

      m->data[0][0] = 1; m->data[0][1] = 2; m->data[0][2] = 1;
      m->data[1][0] = 1; m->data[1][1] = 2; m->data[1][2] = 1;

      e = mat_add(m, n);
      asserteq(e, MATH_NOERR);
      asserteq(m->data[0][0], 2.0); asserteq(m->data[0][1], -3.0); asserteq(m->data[0][2], 3.0);
      asserteq(m->data[1][0], 4.0); asserteq(m->data[1][1],  8.0); asserteq(m->data[1][2], 3.0);
    });

    mat_free(m); mat_free(n);
  });

  subdesc(mat_sub, {
    mat_init(m, 2, 3);

    it("Catches NULL pointers", {
      e = mat_sub(NULL, NULL);
      asserteq(e, MATH_INVARG);

      e = mat_sub(m, NULL);
      asserteq(e, MATH_INVARG);

      e = mat_sub(NULL, m);
      asserteq(e, MATH_INVARG);
    });

    mat_free(m);
    mat_init(n, 2, 2);

    it("Catches already free'd matricies", {
      e = mat_sub(m, n);
      asserteq(e, MATH_INVARG);
      e = mat_sub(n, m);
      asserteq(e, MATH_INVARG);
    });

    mat_init(m, 3, 2);

    it("Catches dimension mismatches", {
      e = mat_sub(m, n);
      asserteq(e, MATH_DIMENSION);
      e = mat_sub(n, m);
      asserteq(e, MATH_DIMENSION);

      mat_free(m);
      mat_init(m, 2, 3);

      e = mat_sub(m, n);
      asserteq(e, MATH_DIMENSION);
      e = mat_sub(n, m);
      asserteq(e, MATH_DIMENSION);
    });

    mat_free(n);
    mat_init(n, 2, 3);

    it("Subtracts matricies", {
      n->data[0][0] = 1; n->data[0][1] = -5; n->data[0][2] = 2;
      n->data[1][0] = 3; n->data[1][1] =  6; n->data[1][2] = 2;

      m->data[0][0] = 1; m->data[0][1] = 2; m->data[0][2] = 1;
      m->data[1][0] = 1; m->data[1][1] = 2; m->data[1][2] = 1;

      e = mat_sub(m, n);
      asserteq(e, MATH_NOERR);
      asserteq(m->data[0][0],  0.0); asserteq(m->data[0][1],  7.0); asserteq(m->data[0][2], -1.0);
      asserteq(m->data[1][0], -2.0); asserteq(m->data[1][1], -4.0); asserteq(m->data[1][2], -1.0);
    });

    mat_free(m); mat_free(n);
  });

  subdesc(mat_pow, {
    it("Catches NULL Pointers", {
      e = mat_pow(NULL, 0);
      asserteq(e, MATH_INVARG);
    });

    it("Catches already free matricies", {
      mat_init(m, 2, 2);
      mat_free(m);
      e = mat_pow(m, 0);
      asserteq(e, MATH_INVARG);
    });

    it("Catches dimension errors", {
      mat_init(m, 2, 3);
      e = mat_pow(m, 0);
      asserteq(e, MATH_DIMENSION);
    });

    it("Catches non-diagnal matricies", {
      mat_init(m, 2, 2);
      defer(mat_free(m););

      m->data[0][0] = 3.0; m->data[0][1] = 1.0;
      m->data[1][0] = 0.0; m->data[1][1] = 2.0;

      e = mat_pow(m, 0);
      asserteq(e, MATH_INVARG);

      m->data[0][1] = 0.0; m->data[1][0] = 1.0;
      e = mat_pow(m, 0);
      asserteq(e, MATH_INVARG);
    });

    it("Operates on diagnal matricies", {
      mat_init(m, 2, 2);
      defer(mat_free(m););

      m->data[0][0] = 3.0; m->data[0][1] = 0.0;
      m->data[1][0] = 0.0; m->data[1][1] = 2.0;

      e = mat_pow(m, 2);
      asserteq(e, MATH_NOERR);
      asserteq(m->data[0][0], 9.0); asserteq(m->data[0][1], 0.0);
      asserteq(m->data[1][0], 0.0); asserteq(m->data[1][1], 4.0);
    });
  });

  subdesc(mat_ident, {
    it("Catches NULL pointers", {
      e = mat_ident(NULL);
      asserteq(e, MATH_INVARG);
    });

    it("Catches already free matricies", {
      mat_init(m, 2, 2);
      mat_free(m);

      e = mat_ident(m);
      asserteq(e, MATH_INVARG);
    });

    it("Catches non-square matricies", {
      mat_init(m, 2, 3);
      defer(mat_free(m););

      e = mat_ident(m);
      asserteq(e, MATH_DIMENSION);
    });

    it("Produces an identity matrix", {
      mat_init(m, 2, 2);
      defer(mat_free(m););

      e = mat_ident(m);
      asserteq(e, MATH_NOERR);
      asserteq(m->data[0][0], 1.0); asserteq(m->data[0][1], 0.0);
      asserteq(m->data[1][0], 0.0); asserteq(m->data[1][1], 1.0);
    });
  });

  subdesc(mat_rowvec , {
    struct math_vec *v = malloc(sizeof(struct math_vec));
    it("Catches NULL pointers", {
      vec_init(v, 2);
      mat_init(m, 2, 2);
      defer({
        vec_free(v);
      });

      e = mat_rowvec(NULL, NULL, 0);
      asserteq(e, MATH_INVARG);
      e = mat_rowvec(v, NULL, 0);
      asserteq(e, MATH_INVARG);
    });

    it("Catches already free'd structs", {
      vec_init(v, 2);
      mat_init(m, 2, 2);
      vec_free(v);
      mat_free(m);

      e = mat_rowvec(v, m, 0);
      asserteq(e, MATH_INVARG);

      vec_init(v, 2);
      defer(vec_free(v););
      e = mat_rowvec(v, m, 0);
      asserteq(e, MATH_INVARG);

      mat_init(m, 2, 2);
      defer({
        vec_init(v, 2);
        mat_free(m);
      });
      vec_free(v);

      e = mat_rowvec(v, m, 0);
      asserteq(e, MATH_INVARG);
    });

    it("Catches dimension errors", {
      vec_init(v, 3);
      mat_init(m, 3, 2);
      defer({
        vec_free(v); mat_free(m);
      });

      e = mat_rowvec(v, m, 0);
      asserteq(e, MATH_DIMENSION);
    });

    it("Catches OOB errors", {
      vec_init(v, 2);
      mat_init(m, 2, 2);
      defer({
        vec_free(v); mat_free(m);
      });

      e = mat_rowvec(v, m, 2);
      asserteq(e, MATH_OOB);
    });

    it("Correctly returns a vector", {
      bool b = false;

      struct math_vec *w = malloc(sizeof(struct math_vec));
      vec_init(w, 2);
      vec_init(v, 2);
      mat_init(m, 2, 2);
      defer({
        vec_free(v); mat_free(m); vec_free(w); free(w);
      });

      m->data[0][0] = 1.0; m->data[0][1] = 4.0;
      m->data[1][0] = 2.0; m->data[1][1] = 3.0;

      w->data[0] = 1.0; w->data[1] = 4.0;

      e = mat_rowvec(v, m, 0);
      asserteq(e, MATH_NOERR);
      vec_eq(&b, v, w);
      assert(b);

      w->data[0] = 2.0; w->data[1] = 3.0;
      e = mat_rowvec(v, m, 1);
      asserteq(e, MATH_NOERR);
      vec_eq(&b, v, w);
      assert(b);
    });

    free(v);
  });

  subdesc(mat_colvec , {
    struct math_vec *v = malloc(sizeof(struct math_vec));
    it("Catches NULL pointers", {
      vec_init(v, 2);
      mat_init(m, 2, 2);
      defer({
        vec_free(v);
      });

      e = mat_colvec(NULL, NULL, 0);
      asserteq(e, MATH_INVARG);
      e = mat_colvec(v, NULL, 0);
      asserteq(e, MATH_INVARG);
    });

    it("Catches already free'd structs", {
      vec_init(v, 2);
      mat_init(m, 2, 2);
      vec_free(v);
      mat_free(m);

      e = mat_colvec(v, m, 0);
      asserteq(e, MATH_INVARG);

      vec_init(v, 2);
      defer(vec_free(v););
      e = mat_colvec(v, m, 0);
      asserteq(e, MATH_INVARG);

      mat_init(m, 2, 2);
      defer({
        vec_init(v, 2);
        mat_free(m);
      });
      vec_free(v);

      e = mat_colvec(v, m, 0);
      asserteq(e, MATH_INVARG);
    });

    it("Catches dimension errors", {
      vec_init(v, 3);
      mat_init(m, 2, 3);
      defer({
        vec_free(v); mat_free(m);
      });

      e = mat_colvec(v, m, 0);
      asserteq(e, MATH_DIMENSION);
    });

    it("Catches OOB errors", {
      vec_init(v, 2);
      mat_init(m, 2, 2);
      defer({
        vec_free(v); mat_free(m);
      });

      e = mat_colvec(v, m, 2);
      asserteq(e, MATH_OOB);
    });

    it("Correctly returns a vector", {
      bool b = false;

      struct math_vec *w = malloc(sizeof(struct math_vec));
      vec_init(w, 2);
      vec_init(v, 2);
      mat_init(m, 2, 2);
      defer({
        vec_free(v); mat_free(m); vec_free(w); free(w);
      });

      m->data[0][0] = 1.0; m->data[0][1] = 4.0;
      m->data[1][0] = 2.0; m->data[1][1] = 3.0;

      w->data[0] = 1.0; w->data[1] = 2.0;

      e = mat_colvec(v, m, 0);
      asserteq(e, MATH_NOERR);
      vec_eq(&b, v, w);
      assert(b);

      w->data[0] = 4.0; w->data[1] = 3.0;
      e = mat_colvec(v, m, 1);
      asserteq(e, MATH_NOERR);
      vec_eq(&b, v, w);
      assert(b);
    });

    free(v);
  });

  subdesc(mat_mult, {
    it("Catches NULL pointers", {
      mat_init(m, 2, 2);
      defer(mat_free(m););

      e = mat_mult(NULL, NULL);
      asserteq(e, MATH_INVARG);
      e = mat_mult(e, NULL);
      asserteq(e, MATH_INVARG);
      e = mat_mult(NULL, m);
      asserteq(e, MATH_INVARG);
    });

    it("Catches already free matricies", {
      mat_init(m, 2, 2);
      mat_init(n, 2, 2);
      mat_free(m);
      defer(mat_free(n));

      e = mat_mult(m, n);
      asserteq(e, MATH_INVARG);
      e = mat_mult(n, m);
      asserteq(e, MATH_INVARG);
    });

    it("Catches dimension mismatch", {
      mat_init(m, 3, 2);
      mat_init(n, 3, 2);
      defer({
        mat_free(m); mat_free(n);
      });

      e = mat_mult(m, n);
      asserteq(e, MATH_DIMENSION);

      mat_free(n);
      mat_init(n, 3, 3);

      e = mat_mult(m, n);
      asserteq(e, MATH_DIMENSION);
      e = mat_mult(n, m);
      asserteq(e, MATH_NOERR);
    });

    it("Multiplies matricies", {
      mat_init(m, 2, 3);
      mat_init(n, 3, 4);
      defer({
        mat_free(n); mat_free(m);
      });

      m->data[0][0] = -3.0; m->data[0][1] = 5.0; m->data[0][2] = 2.0;
      m->data[1][0] =  1.0; m->data[1][1] = 4.0; m->data[1][2] = 4.0;

      n->data[0][0] = 3.0; n->data[0][1] = 0.0; n->data[0][2] = 5.0; n->data[0][3] =  1.0;
      n->data[1][0] = 5.0; n->data[1][1] = 4.0; n->data[1][2] = 5.0; n->data[1][3] = -1.0;
      n->data[2][0] = 7.0; n->data[2][1] = 2.0; n->data[2][2] = 2.0; n->data[2][3] =  1.0;

      e = mat_mult(m, n);
      asserteq(e, MATH_NOERR);
      asserteq(m->row, 2);
      asserteq(m->col, 4);

      asserteq(m->data[0][0], 30.0); asserteq(m->data[0][1], 24.0); asserteq(m->data[0][2], 14.0); asserteq(m->data[0][3], -6.0);
      asserteq(m->data[1][0], 51.0); asserteq(m->data[1][1], 24.0); asserteq(m->data[1][2], 33.0); asserteq(m->data[1][3],  1.0);
    });
  });

  subdesc(mat_operate, {
    struct math_vec *v = malloc(sizeof(struct math_vec));

    it("Catches NULL pointers", {
      vec_init(v, 2);
      mat_init(m, 2, 2);
      defer({
        vec_free(v); mat_free(m);
      });

      e = mat_operate(NULL, NULL);
      asserteq(e, MATH_INVARG);
      e = mat_operate(v, NULL);
      asserteq(e, MATH_INVARG);
      e = mat_operate(NULL, m);
      asserteq(e, MATH_INVARG);
    });

    it("Catches already free'd items", {
      vec_init(v, 2);
      mat_init(m, 2, 2);
      vec_free(v);
      defer(mat_free(m););

      e = mat_operate(v, m);
      asserteq(e, MATH_INVARG);

      mat_free(m);
      vec_init(v, 2);
      defer({
        vec_free(v); mat_init(m, 1, 1);
      });

      e = mat_operate(v, m);
      asserteq(e, MATH_INVARG);
    });

    it("Catches dimensional errors", {
      vec_init(v, 3);
      mat_init(m, 3, 2);
      defer({
        vec_free(v); mat_free(m);
      });

      e = mat_operate(v, m);
      asserteq(e, MATH_DIMENSION);
    });

    it("Performs the operation", {
      struct math_vec *w = malloc(sizeof(struct math_vec));
      vec_init(w, 2);
      vec_init(v, 2);
      mat_init(m, 2, 2);
      defer({
        vec_free(v); mat_free(m); vec_free(w); free(w);
      });

      v->data[0] = 1.0; v->data[1] = 2.0;

      m->data[0][0] = 3.0; m->data[0][1] = 2.0;
      m->data[1][0] = 5.0; m->data[1][1] = 1.0;

      w->data[0] = 7.0; w->data[1] = 7.0;

      e = mat_operate(v, m);
      asserteq(e, MATH_NOERR);
      bool b = false;
      vec_eq(&b, w, v);
      assert(b);
    });

    free(v);
  });

  subdesc(mat_vtocm, {
    struct math_vec *v = malloc(sizeof(struct math_vec));

    it("Catches NULL pointers", {
      vec_init(v, 2);
      mat_init(m, 2, 1);
      defer({
        vec_free(v); mat_free(m);
      });

      e = mat_vtocm(NULL, NULL);
      asserteq(e, MATH_INVARG);
      e = mat_vtocm(m, NULL);
      asserteq(e, MATH_INVARG);
      e = mat_vtocm(NULL, v);
      asserteq(e, MATH_INVARG);
    });

    it("Catches already free'd items", {
      vec_init(v, 2);
      mat_init(m, 2, 2);
      vec_free(v);
      defer(mat_free(m););

      e = mat_vtocm(m, v);
      asserteq(e, MATH_INVARG);

      mat_free(m);
      vec_init(v, 2);
      defer({
        vec_free(v); mat_init(m, 1, 1);
      });

      e = mat_vtocm(m, v);
      asserteq(e, MATH_INVARG);
    });

    it("Catches dimensional errors", {
      vec_init(v, 2);
      mat_init(m, 2, 2);
      defer({
        vec_free(v); mat_free(m);
      });

      e = mat_vtocm(m, v);
      asserteq(e, MATH_DIMENSION);

      mat_free(m);
      mat_init(m, 3, 1);

      e = mat_vtocm(m, v);
      asserteq(e, MATH_DIMENSION);
    });

    it("Transforms a vector", {
      vec_init(v, 2);
      mat_init(m, 2, 1);
      defer({
        vec_free(v); mat_free(m);
      });

      v->data[0] = 1.0; v->data[1] = 2.0;

      e = mat_vtocm(m, v);
      asserteq(e, MATH_NOERR);
      asserteq(m->data[0][0], 1.0);
      asserteq(m->data[1][0], 2.0);
    });

    subdesc(mat_vtorm, {
      struct math_vec *v = malloc(sizeof(struct math_vec));

      it("Catches NULL pointers", {
        vec_init(v, 2);
        mat_init(m, 1, 2);
        defer({
          vec_free(v); mat_free(m);
        });

        e = mat_vtorm(NULL, NULL);
        asserteq(e, MATH_INVARG);
        e = mat_vtorm(m, NULL);
        asserteq(e, MATH_INVARG);
        e = mat_vtorm(NULL, v);
        asserteq(e, MATH_INVARG);
      });

      it("Catches already free'd items", {
        vec_init(v, 2);
        mat_init(m, 1, 2);
        vec_free(v);
        defer(mat_free(m););

        e = mat_vtorm(m, v);
        asserteq(e, MATH_INVARG);

        mat_free(m);
        vec_init(v, 2);
        defer({
          vec_free(v); mat_init(m, 1, 1);
        });

        e = mat_vtorm(m, v);
        asserteq(e, MATH_INVARG);
      });

      it("Catches dimensional errors", {
        vec_init(v, 2);
        mat_init(m, 2, 2);
        defer({
          vec_free(v); mat_free(m);
        });

        e = mat_vtorm(m, v);
        asserteq(e, MATH_DIMENSION);

        mat_free(m);
        mat_init(m, 1, 3);

        e = mat_vtorm(m, v);
        asserteq(e, MATH_DIMENSION);
      });

      it("Transforms a vector", {
        vec_init(v, 2);
        mat_init(m, 1, 2);
        defer({
          vec_free(v); mat_free(m);
        });

        v->data[0] = 1.0; v->data[1] = 2.0;

        e = mat_vtorm(m, v);
        asserteq(e, MATH_NOERR);
        asserteq(m->data[0][0], 1.0);
        asserteq(m->data[0][1], 2.0);
      });
    });

    subdesc(mat_setrow, {
      struct math_vec *v = malloc(sizeof(struct math_vec));

      it("Catches NULL pointers", {
        vec_init(v, 2);
        mat_init(m, 1, 2);
        defer({
          vec_free(v); mat_free(m);
        });

        e = mat_setrow(NULL, NULL, 0);
        asserteq(e, MATH_INVARG);
        e = mat_setrow(m, NULL, 0);
        asserteq(e, MATH_INVARG);
        e = mat_setrow(NULL, v, 0);
        asserteq(e, MATH_INVARG);
      });

      it("Catches already free'd items", {
        vec_init(v, 2);
        mat_init(m, 1, 2);
        vec_free(v);
        defer(mat_free(m););

        e = mat_setrow(m, v, 0);
        asserteq(e, MATH_INVARG);

        mat_free(m);
        vec_init(v, 2);
        defer({
          vec_free(v); mat_init(m, 1, 1);
        });

        e = mat_setrow(m, v, 0);
        asserteq(e, MATH_INVARG);
      });

      it("Catches dimensional errors", {
        vec_init(v, 3);
        mat_init(m, 3, 2);
        defer({
          vec_free(v); mat_free(m);
        });

        e = mat_setrow(m, v, 0);
        asserteq(e, MATH_DIMENSION);
      });

      it("Catches OOB erros", {
        vec_init(v, 2);
        mat_init(m, 2, 2);
        defer({
          vec_free(v); mat_free(m);
        });

        e = mat_setrow(m, v, 2);
        asserteq(e, MATH_OOB);
      });

      it("Sets the matrix row", {
        vec_init(v, 2);
        mat_init(m, 2, 2);
        defer({
          vec_free(v); mat_free(m);
        });

        v->data[0] = 1.0; v->data[1] = 2.0;

        e = mat_setrow(m, v, 0);
        asserteq(m->data[0][0], 1.0); asserteq(m->data[0][1], 2.0);
      });

      free(v);
    });

    subdesc(mat_setcol, {
      struct math_vec *v = malloc(sizeof(struct math_vec));

      it("Catches NULL pointers", {
        vec_init(v, 2);
        mat_init(m, 1, 2);
        defer({
          vec_free(v); mat_free(m);
        });

        e = mat_setcol(NULL, NULL, 0);
        asserteq(e, MATH_INVARG);
        e = mat_setcol(m, NULL, 0);
        asserteq(e, MATH_INVARG);
        e = mat_setcol(NULL, v, 0);
        asserteq(e, MATH_INVARG);
      });

      it("Catches already free'd items", {
        vec_init(v, 2);
        mat_init(m, 1, 2);
        vec_free(v);
        defer(mat_free(m););

        e = mat_setcol(m, v, 0);
        asserteq(e, MATH_INVARG);

        mat_free(m);
        vec_init(v, 2);
        defer({
          vec_free(v); mat_init(m, 1, 1);
        });

        e = mat_setcol(m, v, 0);
        asserteq(e, MATH_INVARG);
      });

      it("Catches dimensional errors", {
        vec_init(v, 3);
        mat_init(m, 2, 3);
        defer({
          vec_free(v); mat_free(m);
        });

        e = mat_setcol(m, v, 0);
        asserteq(e, MATH_DIMENSION);
      });

      it("Catches OOB erros", {
        vec_init(v, 2);
        mat_init(m, 2, 2);
        defer({
          vec_free(v); mat_free(m);
        });

        e = mat_setcol(m, v, 2);
        asserteq(e, MATH_OOB);
      });

      it("Sets the matrix row", {
        vec_init(v, 2);
        mat_init(m, 2, 2);
        defer({
          vec_free(v); mat_free(m);
        });

        v->data[0] = 1.0; v->data[1] = 2.0;

        e = mat_setcol(m, v, 0);
        asserteq(m->data[0][0], 1.0); asserteq(m->data[1][0], 2.0);
      });

      free(v);
    });

    subdesc(mat_transpose, {
      it("Catches NULL pointers", {
        e = mat_transpose(NULL);
        asserteq(e, MATH_INVARG);
      });

      it("Catches already free'd matricies", {
        mat_init(m, 1, 1);
        mat_free(m);

        e = mat_transpose(m);
        asserteq(e, MATH_INVARG);
      });

      it("Finds the correct transpose", {
        mat_init(m, 2, 3);

        m->data[0][0] = 1.0; m->data[0][1] = 2.0; m->data[0][2] = 3.0;
        m->data[1][0] = 4.0; m->data[1][1] = 5.0; m->data[1][2] = 6.0;

        e = mat_transpose(m);
        asserteq(e, MATH_NOERR);
        asserteq(m->row, 3);
        asserteq(m->col, 2);
        asserteq(m->data[0][0], 1.0); asserteq(m->data[0][1], 4.0);
        asserteq(m->data[1][0], 2.0); asserteq(m->data[1][1], 5.0);
        asserteq(m->data[2][0], 3.0); asserteq(m->data[2][2], 6.0);
      });
    });

    free(v);
  });

  subdesc(mat_det, {
    double d = 0.0;

    it("Catches NULL pointers", {
      mat_init(m, 2, 2);
      defer(mat_free(m););

      e = mat_det(NULL, NULL);
      asserteq(e, MATH_INVARG);
      e = mat_det(NULL, m);
      asserteq(e, MATH_INVARG);
      e = mat_det(&d, NULL);
      asserteq(e, MATH_INVARG);
    });

    it("Catches already free'd matricies", {
      mat_init(m, 1, 1);
      mat_free(m);

      e = mat_det(&d, m);
      asserteq(e, MATH_INVARG);
    });

    it("Catches non-square matricies", {
      mat_init(m, 2, 3);
      defer(mat_free(m););

      e = mat_det(&d, m);
      asserteq(e, MATH_DIMENSION);
    });

    it("Finds the determinate", {
      mat_init(m, 1, 1);
      defer(mat_free(m););
      m->data[0][0] = 1.0;

      e = mat_det(&d, m);
      asserteq(e, MATH_NOERR);
      asserteq(d, 1.0);

      mat_free(m);
      mat_init(m, 2, 2);

      m->data[0][0] = 1.0; m->data[0][1] = 2.0;
      m->data[1][0] = 2.0; m->data[1][1] = 4.0;

      e = mat_det(&d, m);
      asserteq(e, MATH_NOERR);
      asserteq(d, 0.0);

      mat_free(m);
      mat_init(m, 3, 3);

      m->data[0][0] = 1.0; m->data[0][1] = 2.0; m->data[0][2] = 1.0;
      m->data[1][0] = 2.0; m->data[1][1] = 7.0; m->data[1][2] = 3.0;
      m->data[2][0] = 2.0; m->data[2][1] = 4.0; m->data[2][2] = 5.0;

      e = mat_det(&d, m);
      asserteq(e, MATH_NOERR);
      asserteq(d, 9.0);

      mat_free(m);
      mat_init(m, 4, 4);

      m->data[0][0] = 1.0; m->data[0][1] = 2.0; m->data[0][2] = 1.0; m->data[0][3] = 1.0;
      m->data[1][0] = 2.0; m->data[1][1] = 6.0; m->data[1][2] = 3.0; m->data[1][3] = 1.0;
      m->data[2][0] = 2.0; m->data[2][1] = 4.0; m->data[2][2] = 4.0; m->data[2][3] = 1.0;
      m->data[3][0] = 3.0; m->data[3][1] = 4.0; m->data[3][2] = 4.0; m->data[3][3] = 1.0;

      e = mat_det(&d, m);
      asserteq(e, MATH_NOERR);
      asserteq(d, -8.0);
    });
  });

  subdesc(mat_inv, {
    it("Catches NULL pointers", {
      e = mat_inv(NULL);
      asserteq(e, MATH_INVARG);
    });

    it("Catches already free matricies", {
      mat_init(m, 2, 2);
      mat_free(m);

      e = mat_inv(m);
      asserteq(e, MATH_INVARG);
    });

    it("Catches non-square matricies", {
      mat_init(m, 2, 3);
      defer(mat_free(m););

      e = mat_inv(m);
      asserteq(e, MATH_DIMENSION);
    });

    it("Catches non-invertable matricies", {
      mat_init(m, 2, 2);
      defer(mat_free(m););

      m->data[0][0] = 1.0; m->data[0][1] = 2.0;
      m->data[1][0] = 2.0; m->data[1][1] = 4.0;

      e = mat_inv(m);
      asserteq(e, MATH_DIVZ);
    });

    it("Finds the inverses of matricies", {
      mat_init(m, 1, 1);
      defer(mat_free(m););
      m->data[0][0] = 2.0;

      e = mat_inv(m);
      asserteq(e, MATH_NOERR);
      asserteq(m->data[0][0], 0.5);

      mat_free(m);
      mat_init(m, 2, 2);

      m->data[0][0] = 4.0; m->data[0][1] = 2.0;
      m->data[1][0] = 4.0; m->data[1][1] = 4.0;

      e = mat_inv(m);
      asserteq(e, MATH_NOERR);
      asserteq(m->data[0][0],  0.5); asserteq(m->data[0][1], -0.25);
      asserteq(m->data[1][0], -0.5); asserteq(m->data[1][1],  0.50);

      mat_free(m);
      mat_init(m, 3, 3);

      m->data[0][0] = 1.0; m->data[0][1] = 2.0; m->data[0][2] = 3.0;
      m->data[1][0] = 0.0; m->data[1][1] = 1.0; m->data[1][2] = 4.0;
      m->data[2][0] = 5.0; m->data[2][1] = 6.0; m->data[2][2] = 0.0;

      e = mat_inv(m);
      asserteq(e, MATH_NOERR);
      asserteq(m->data[0][0], -24.0); asserteq(m->data[0][1],  18.0); asserteq(m->data[0][2],  5.0);
      asserteq(m->data[1][0],  20.0); asserteq(m->data[1][1], -15.0); asserteq(m->data[1][2], -4.0);
      asserteq(m->data[2][0],  -5.0); asserteq(m->data[2][1],   4.0); asserteq(m->data[2][2],  1.0);

      mat_free(m);
      mat_init(m, 4, 4);

      m->data[0][0] = 1.0; m->data[0][1] = 2.0; m->data[0][2] = 1.0; m->data[0][3] = 1.0;
      m->data[1][0] = 2.0; m->data[1][1] = 6.0; m->data[1][2] = 3.0; m->data[1][3] = 1.0;
      m->data[2][0] = 2.0; m->data[2][1] = 4.0; m->data[2][2] = 4.0; m->data[2][3] = 1.0;
      m->data[3][0] = 3.0; m->data[3][1] = 4.0; m->data[3][2] = 4.0; m->data[3][3] = 1.0;

      e = mat_inv(m);
      asserteq(e, MATH_NOERR);
      asserteq(m->data[0][0],  0.000); asserteq(m->data[0][1],  0.000); asserteq(m->data[0][2], -1.000); asserteq(m->data[0][3],  1.000);
      asserteq(m->data[1][0], -0.125); asserteq(m->data[1][1],  0.375); asserteq(m->data[1][2], -0.125); asserteq(m->data[1][3], -0.125);
      asserteq(m->data[2][0], -0.250); asserteq(m->data[2][1], -0.250); asserteq(m->data[2][2],  0.750); asserteq(m->data[2][3], -0.250);
      asserteq(m->data[3][0],  1.500); asserteq(m->data[3][1], -0.500); asserteq(m->data[3][2],  0.500); asserteq(m->data[3][3], -0.500);
    });
  });

  subdesc(mat_rowop, {
    it("Catches NULL pointers", {
      e = mat_rowop(NULL, 0, 0, 0.0, 0.0);
      asserteq(e, MATH_INVARG);
    });

    it("Catches already free matricies", {
      mat_init(m, 2, 2);
      mat_free(m);

      e = mat_rowop(m, 0, 0, 0.0, 0.0);
      asserteq(e, MATH_INVARG);
    });

    it("Catches OOB errors", {
      mat_init(m, 2, 2);
      defer(mat_free(m););

      e = mat_rowop(m, 2, 0, 0.0, 0.0);
      asserteq(e, MATH_OOB);
      e = mat_rowop(m, 0, 2, 0.0, 0.0);
      asserteq(e, MATH_OOB);
    });

    it("Performs an elementary row operation", {
      mat_init(m, 3, 3);
      defer(mat_free(m););

      m->data[0][0] =  2.0; m->data[0][1] =  1.0; m->data[0][2] = 1.0;
      m->data[1][0] = -3.0; m->data[1][1] = -1.0; m->data[1][2] = 2.0;
      m->data[2][0] = -2.0; m->data[2][1] =  1.0; m->data[2][2] = 2.0;

      e = mat_rowop(m, 0, 1, 2.0, 3.0);
      asserteq(e, MATH_NOERR);
      asserteq(m->data[0][0], -5.0); asserteq(m->data[0][1], -1.0); asserteq(m->data[0][2], 8.0);
      asserteq(m->data[1][0], -3.0); asserteq(m->data[1][1], -1.0); asserteq(m->data[1][2], 2.0);
      asserteq(m->data[2][0], -2.0); asserteq(m->data[2][1],  1.0); asserteq(m->data[2][2], 2.0);
    });
  });

  subdesc(mat_rowswap, {
    it("Catches NULL pointers", {
      e = mat_rowswap(NULL, 0, 0);
      asserteq(e, MATH_INVARG);
    });

    it("Catches already free matricies", {
      mat_init(m, 2, 2);
      mat_free(m);

      e = mat_rowswap(m, 0, 0);
      asserteq(e, MATH_INVARG);
    });

    it("Catches OOB errors", {
      mat_init(m, 2, 2);
      defer(mat_free(m););

      e = mat_rowswap(m, 2, 0);
      asserteq(e, MATH_OOB);
      e = mat_rowswap(m, 0, 2);
      asserteq(e, MATH_OOB);
    });

    it("Performs an row swap", {
      mat_init(m, 3, 3);
      defer(mat_free(m););

      m->data[0][0] =  2.0; m->data[0][1] =  1.0; m->data[0][2] = 1.0;
      m->data[1][0] = -3.0; m->data[1][1] = -1.0; m->data[1][2] = 2.0;
      m->data[2][0] = -2.0; m->data[2][1] =  1.0; m->data[2][2] = 2.0;

      e = mat_rowswap(m, 0, 1);
      asserteq(e, MATH_NOERR);
      asserteq(m->data[0][0], -3.0); asserteq(m->data[0][1], -1.0); asserteq(m->data[0][2], 2.0);
      asserteq(m->data[1][0],  2.0); asserteq(m->data[1][1],  1.0); asserteq(m->data[1][2], 1.0);
      asserteq(m->data[2][0], -2.0); asserteq(m->data[2][1],  1.0); asserteq(m->data[2][2], 2.0);
    });
  });

  subdesc(mat_ref, {
    double d = 0.0;

    it("Catches NULL pointers", {
      e = mat_ref(NULL, NULL);
      asserteq(e, MATH_INVARG);
      e = mat_ref(NULL, &d);
      asserteq(e, MATH_INVARG);
    });

    it("Catches already free matricies", {
      mat_init(m, 2, 2);
      mat_free(m);

      e = mat_ref(m, NULL);
      asserteq(e, MATH_INVARG);
      e = mat_ref(m, &d);
      asserteq(e, MATH_INVARG);
    });

    it("Finds the row echelon form", {
      mat_init(m, 3, 3);
      defer(mat_free(m););

      m->data[0][0] =  2.0; m->data[0][1] =  1.0; m->data[0][2] = -1.0;
      m->data[1][0] = -3.0; m->data[1][1] = -1.0; m->data[1][2] = 2.0;
      m->data[2][0] = -2.0; m->data[2][1] =  1.0; m->data[2][2] = 2.0;

      e = mat_ref(m, &d);
      asserteq(e, MATH_NOERR);
      asserteq(m->data[0][0],  2.0); asserteq(m->data[0][1],  0.0); asserteq(m->data[0][2], 0.0);
      asserteq(m->data[1][0],  0.0); asserteq(m->data[1][1],  0.5); asserteq(m->data[1][2],  0.0);
      asserteq(m->data[2][0],  0.0); asserteq(m->data[2][1],  0.0); asserteq(m->data[2][2], -1.0);
      asserteq(d, 1.0);
    });
  });

  subdesc(mat_rref, {
    double d = 0.0;

    it("Catches NULL pointers", {
      e = mat_rref(NULL, NULL);
      asserteq(e, MATH_INVARG);
      e = mat_rref(NULL, &d);
      asserteq(e, MATH_INVARG);
    });

    it("Catches already free matricies", {
      mat_init(m, 2, 2);
      mat_free(m);

      e = mat_rref(m, NULL);
      asserteq(e, MATH_INVARG);
      e = mat_rref(m, &d);
      asserteq(e, MATH_INVARG);
    });

    it("Finds the reduced row echelon form", {
      mat_init(m, 3, 3);
      defer(mat_free(m););

      m->data[0][0] =  2.0; m->data[0][1] =  1.0; m->data[0][2] = -1.0;
      m->data[1][0] = -3.0; m->data[1][1] = -1.0; m->data[1][2] = 2.0;
      m->data[2][0] = -2.0; m->data[2][1] =  1.0; m->data[2][2] = 2.0;

      e = mat_rref(m, &d);
      asserteq(e, MATH_NOERR);
      asserteq(m->data[0][0], 1.0); asserteq(m->data[0][1], 0.0); asserteq(m->data[0][2], 0.0);
      asserteq(m->data[1][0], 0.0); asserteq(m->data[1][1], 1.0); asserteq(m->data[1][2], 0.0);
      asserteq(m->data[2][0], 0.0); asserteq(m->data[2][1], 0.0); asserteq(m->data[2][2], 1.0);
      asserteq(d, -1.0);
    });
  });

  subdesc(mat_submatrix, {
    it("Catches NULL pointers", {
      mat_init(m, 1, 1);
      defer(mat_free(m););

      e = mat_submatrix(NULL, NULL, 0, 0);
      asserteq(e, MATH_INVARG);
      e = mat_submatrix(m, NULL, 0, 0);
      asserteq(e, MATH_INVARG);
      e = mat_submatrix(NULL, m, 0, 0);
      asserteq(e, MATH_INVARG);
    });

    it("Catches already free'd matricies", {
      mat_init(m, 2, 2);
      mat_free(m);
      mat_init(n, 1, 1);
      defer(mat_free(n););

      e = mat_submatrix(n, m, 0, 0);
      asserteq(e, MATH_INVARG);
      e = mat_submatrix(m, n, 0, 0);
      asserteq(e, MATH_INVARG);
    });

    it("Catches dimensional errors", {
      mat_init(m, 2, 2);
      mat_init(n, 2, 2);
      defer({
        mat_free(m); mat_free(n);
      });

      e = mat_submatrix(m, n, 0, 0);
      asserteq(e, MATH_DIMENSION);

      mat_free(n);
      mat_init(n, 3, 3);

      e = mat_submatrix(n, m, 0, 0);
      asserteq(e, MATH_DIMENSION);
    });

    it("Catches OOB errors", {
      mat_init(m, 2, 2);
      mat_init(n, 3, 3);
      defer({
        mat_free(m); mat_free(n);
      });

      e = mat_submatrix(m, n, 3, 0);
      asserteq(e, MATH_OOB);
      e = mat_submatrix(m, n, 0, 3);
      asserteq(e, MATH_OOB);
    });

    it("Correctly submatricies", {
      mat_init(m, 2, 2);
      mat_init(n, 3, 3);
      defer({
        mat_free(m); mat_free(n);
      });

      n->data[0][0] = 1.0; n->data[0][1] = 2.0; n->data[0][2] = 3.0;
      n->data[1][0] = 4.0; n->data[1][1] = 5.0; n->data[1][2] = 6.0;
      n->data[2][0] = 7.0; n->data[2][1] = 8.0; n->data[2][2] = 9.0;

      e = mat_submatrix(m, n, 0, 0);
      asserteq(e, MATH_NOERR);
      asserteq(m->data[0][0], 5.0); asserteq(m->data[0][1], 6.0);
      asserteq(m->data[1][0], 8.0); asserteq(m->data[1][1], 9.0);

      e = mat_submatrix(m, n, 1, 1);
      asserteq(e, MATH_NOERR);
      asserteq(m->data[0][0], 1.0); asserteq(m->data[0][1], 3.0);
      asserteq(m->data[1][0], 7.0); asserteq(m->data[1][1], 9.0);
    });
  });

  subdesc(mat_submatrix2, {
    it("Catches NULL pointers", {
      mat_init(m, 1, 1);
      defer(mat_free(m););

      e = mat_submatrix2(NULL, NULL, 0, 0, 0, 0);
      asserteq(e, MATH_INVARG);
      e = mat_submatrix2(m, NULL, 0, 0, 0, 0);
      asserteq(e, MATH_INVARG);
      e = mat_submatrix2(NULL, m, 0, 0, 0, 0);
      asserteq(e, MATH_INVARG);
    });

    it("Catches already free'd matricies", {
      mat_init(m, 2, 2);
      mat_free(m);
      mat_init(n, 1, 1);
      defer(mat_free(n););

      e = mat_submatrix2(n, m, 0, 0, 0, 0);
      asserteq(e, MATH_INVARG);
      e = mat_submatrix2(m, n, 0, 0, 0, 0);
      asserteq(e, MATH_INVARG);
    });

    it("Catches dimensional errors", {
      mat_init(m, 2, 2);
      mat_init(n, 2, 2);
      defer({
        mat_free(m); mat_free(n);
      });

      e = mat_submatrix2(m, n, 0, 0, 0, 0);
      asserteq(e, MATH_DIMENSION);

      mat_free(n);
      mat_init(n, 3, 3);

      e = mat_submatrix2(n, m, 0, 0, 0, 0);
      asserteq(e, MATH_DIMENSION);

      mat_free(n);
      mat_init(n, 1, 1);

      e = mat_submatrix2(n, m, 0, 1, 0, 0);
      asserteq(e, MATH_DIMENSION);
    });

    it("Catches OOB errors", {
      mat_init(m, 2, 2);
      mat_init(n, 3, 3);
      defer({
        mat_free(m); mat_free(n);
      });

      e = mat_submatrix2(m, n, 2, 3, 0, 0);
      asserteq(e, MATH_OOB);
      e = mat_submatrix2(m, n, 0, 0, 2, 3);
      asserteq(e, MATH_OOB);
    });

    it("Correctly submatricies", {
      mat_init(m, 2, 2);
      mat_init(n, 3, 3);
      defer({
        mat_free(m); mat_free(n);
      });

      n->data[0][0] = 1.0; n->data[0][1] = 2.0; n->data[0][2] = 3.0;
      n->data[1][0] = 4.0; n->data[1][1] = 5.0; n->data[1][2] = 6.0;
      n->data[2][0] = 7.0; n->data[2][1] = 8.0; n->data[2][2] = 9.0;

      e = mat_submatrix2(m, n, 1, 2, 1, 2);
      asserteq(e, MATH_NOERR);
      asserteq(m->data[0][0], 5.0); asserteq(m->data[0][1], 6.0);
      asserteq(m->data[1][0], 8.0); asserteq(m->data[1][1], 9.0);

      e = mat_submatrix2(m, n, 0, 1, 1, 2);
      asserteq(e, MATH_NOERR);
      asserteq(m->data[0][0], 4.0); asserteq(m->data[0][1], 5.0);
      asserteq(m->data[1][0], 7.0); asserteq(m->data[1][1], 8.0);

      mat_free(m);
      mat_init(m, 2, 1);

      e = mat_submatrix2(m, n, 0, 0, 1, 2);
      asserteq(e, MATH_NOERR);
      asserteq(m->data[0][0], 4.0);
      asserteq(m->data[1][0], 7.0);
    });
  });

  subdesc(mat_minor, {
    it("Catches NULL pointers", {
      e = mat_minor(NULL);
      asserteq(e, MATH_INVARG);
    });

    it("Catches already free'd matricies", {
      mat_init(m, 2, 2);
      mat_free(m);

      e = mat_minor(m);
      asserteq(e, MATH_INVARG);
    });

    it("Catches non-square matricies", {
      mat_init(m, 2, 3);
      defer(mat_free(m););

      e = mat_minor(m);
      asserteq(e, MATH_DIMENSION);
    });

    it("Finds the matrix of minors", {
      mat_init(m, 1, 1);
      defer(mat_free(m););

      m->data[0][0] = 1.0;
      e = mat_minor(m);
      asserteq(e, MATH_NOERR);
      asserteq(m->data[0][0], 1.0);

      mat_free(m);
      mat_init(m, 2, 2);

      m->data[0][0] = 1.0; m->data[0][1] = 2.0;
      m->data[1][0] = 3.0; m->data[1][1] = 4.0;

      e = mat_minor(m);
      asserteq(e, MATH_NOERR);
      asserteq(m->data[0][0], 4.0); asserteq(m->data[0][1], 3.0);
      asserteq(m->data[1][0], 2.0); asserteq(m->data[1][1], 1.0);

      mat_free(m);
      mat_init(m, 3, 3);

      m->data[0][0] = 3.0; m->data[0][1] = 5.0; m->data[0][2] = 3.0;
      m->data[1][0] = 6.0; m->data[1][1] = 2.0; m->data[1][2] = 2.0;
      m->data[2][0] = 1.0; m->data[2][1] = 1.0; m->data[2][2] = 4.0;

      e = mat_minor(m);
      asserteq(e, MATH_NOERR);
      asserteq(m->data[0][0],  6.0); asserteq(m->data[0][1],  22.0); asserteq(m->data[0][2],   4.0);
      asserteq(m->data[1][0], 17.0); asserteq(m->data[1][1],   9.0); asserteq(m->data[1][2],  -2.0);
      asserteq(m->data[2][0],  4.0); asserteq(m->data[2][1], -12.0); asserteq(m->data[2][2], -24.0);
    });
  });

  subdesc(mat_adj, {
    it("Catches NULL pointers", {
      e = mat_adj(NULL);
      asserteq(e, MATH_INVARG);
    });

    it("Catches already free'd matricies", {
      mat_init(m, 2, 2);
      mat_free(m);

      e = mat_adj(m);
      asserteq(e, MATH_INVARG);
    });

    it("Catches non-square matricies", {
      mat_init(m, 2, 3);
      defer(mat_free(m););

      e = mat_adj(m);
      asserteq(e, MATH_DIMENSION);
    });

    it("Finds the adjoint matrix", {
      mat_init(m, 1, 1);
      defer(mat_free(m););

      m->data[0][0] = 1.0;
      e = mat_adj(m);
      asserteq(e, MATH_NOERR);
      asserteq(m->data[0][0], 1.0);

      mat_free(m);
      mat_init(m, 2, 2);

      m->data[0][0] = 1.0; m->data[0][1] = 2.0;
      m->data[1][0] = 3.0; m->data[1][1] = 4.0;

      e = mat_adj(m);
      asserteq(e, MATH_NOERR);
      asserteq(m->data[0][0],  4.0); asserteq(m->data[1][0], -3.0);
      asserteq(m->data[0][1], -2.0); asserteq(m->data[1][1],  1.0);

      mat_free(m);
      mat_init(m, 3, 3);

      m->data[0][0] = 3.0; m->data[0][1] = 5.0; m->data[0][2] = 3.0;
      m->data[1][0] = 6.0; m->data[1][1] = 2.0; m->data[1][2] = 2.0;
      m->data[2][0] = 1.0; m->data[2][1] = 1.0; m->data[2][2] = 4.0;

      e = mat_adj(m);
      asserteq(e, MATH_NOERR);
      asserteq(m->data[0][0],   6.0); asserteq(m->data[1][0], -22.0); asserteq(m->data[2][0],   4.0);
      asserteq(m->data[0][1], -17.0); asserteq(m->data[1][1],   9.0); asserteq(m->data[2][1],   2.0);
      asserteq(m->data[0][2],   4.0); asserteq(m->data[1][2],  12.0); asserteq(m->data[2][2], -24.0);
    });
  });

  subdesc(mat_eq, {
    bool b = false;
    it("Catches NULL pointers", {
      mat_init(m, 1, 1);
      defer(mat_free(m););

      e = mat_eq(NULL, NULL, NULL);
      asserteq(e, MATH_INVARG);
      e = mat_eq(NULL, m, NULL);
      asserteq(e, MATH_INVARG);
      e = mat_eq(NULL, NULL, m);
      asserteq(e, MATH_INVARG);
      e = mat_eq(&b, NULL, NULL);
      asserteq(e, MATH_INVARG);
      e = mat_eq(&b, m, NULL);
      asserteq(e, MATH_INVARG);
      e = mat_eq(&b, NULL, m);
      asserteq(e, MATH_INVARG);
    });

    it("Catches already free'd matricies", {
      mat_init(m, 1, 1);
      mat_init(n, 1, 1);
      defer(mat_free(m););
      mat_free(n);

      e = mat_eq(&b, m, n);
      asserteq(e, MATH_INVARG);
      e = mat_eq(&b, n, m);
      asserteq(e, MATH_INVARG);
    });

    it("Catches dimension errors", {
      mat_init(m, 2, 2);
      mat_init(n, 2, 3);
      defer({
        mat_free(m); mat_free(n);
      });

      e = mat_eq(&b, m, n);
      asserteq(e, MATH_DIMENSION);
      e = mat_eq(&b, n, m);
      asserteq(e, MATH_DIMENSION);

      mat_free(n);
      mat_init(n, 3, 2);

      e = mat_eq(&b, m, n);
      asserteq(e, MATH_DIMENSION);
      e = mat_eq(&b, n, m);
      asserteq(e, MATH_DIMENSION);
    });

    it("Tests equality", {
      mat_init(m, 2, 2);
      mat_init(n, 2, 2);
      defer({
        mat_free(m); mat_free(n);
      });

      m->data[0][0] = 1.0; m->data[0][1] = 2.0;
      m->data[1][0] = 3.0; m->data[1][1] = 4.0;

      e = mat_eq(&b, m, n);
      asserteq(e, MATH_NOERR);
      assert(!b);
      e = mat_eq(&b, n, m);
      asserteq(e, MATH_NOERR);
      assert(!b);

      n->data[0][0] = 1.0;

      e = mat_eq(&b, m, n);
      asserteq(e, MATH_NOERR);
      assert(!b);
      e = mat_eq(&b, n, m);
      asserteq(e, MATH_NOERR);
      assert(!b);

      n->data[0][1] = 2.0;

      e = mat_eq(&b, m, n);
      asserteq(e, MATH_NOERR);
      assert(!b);
      e = mat_eq(&b, n, m);
      asserteq(e, MATH_NOERR);
      assert(!b);

      n->data[1][0] = 3.0;

      e = mat_eq(&b, m, n);
      asserteq(e, MATH_NOERR);
      assert(!b);
      e = mat_eq(&b, n, m);
      asserteq(e, MATH_NOERR);
      assert(!b);

      n->data[1][1] = 4.0;

      e = mat_eq(&b, m, n);
      asserteq(e, MATH_NOERR);
      assert(b);
      e = mat_eq(&b, n, m);
      asserteq(e, MATH_NOERR);
      assert(b);
    });
  });

  subdesc(mat_cp, {
    it("Catches NULL pointers", {
      mat_init(m, 1, 1);
      defer(mat_free(m););

      e = mat_cp(NULL, NULL);
      asserteq(e, MATH_INVARG);
      e = mat_cp(m, NULL);
      asserteq(e, MATH_INVARG);
      e = mat_cp(NULL, m);
      asserteq(e, MATH_INVARG);
    });

    it("Catches already free'd matricies", {
      mat_init(m, 2, 2);
      mat_init(n, 2, 2);
      defer(mat_free(n);); mat_free(n);

      e = mat_cp(m, n);
      asserteq(e, MATH_INVARG);
      e = mat_cp(n, m);
      asserteq(e, MATH_INVARG);
    });

    it("Catches dimension errors", {
      mat_init(m, 2, 2);
      mat_init(n, 3, 2);
      defer({
        mat_free(n); mat_free(m);
      });

      e = mat_cp(n, m);
      asserteq(e, MATH_DIMENSION);

      e = mat_cp(n, m);
      asserteq(e, MATH_DIMENSION);
    });

    it("Copies a matrix", {
      mat_init(m, 2, 2);
      mat_init(n, 2, 2);
      defer({
        mat_free(n); mat_free(m);
      });

      m->data[0][0] = 1.0; m->data[0][1] = 2.0;
      m->data[1][0] = 3.0; m->data[1][1] = 4.0;

      bool b = false;
      mat_eq(&b, n, m);
      assert(!b);

      e = mat_cp(n, m);
      asserteq(e, MATH_NOERR);
      mat_eq(&b, n, m);
      assert(b);
      assertneq(m->data[0][0], 0.0);
    });
  });

  free(m);
  free(n);
});

snow_main();
