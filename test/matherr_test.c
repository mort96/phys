#include "math/matherror.h"

#include <stdlib.h>
#include <stdio.h>

#include "snow.h"

describe(matherror, {
  subdesc(math_perrorf, {
    it("Doesn't crash on any print", {
      MATH_ERROR m;
      FILE *dnull = fopen("/dev/null", "w");
      assert(dnull);
      defer(fclose(dnull));
      m = math_perrorf(MATH_NOERR, dnull);
      asserteq(m, MATH_NOERR);
      m = math_perrorf(MATH_SHRTBUF, dnull);
      asserteq(m, MATH_NOERR);
      m = math_perrorf(MATH_INVARG, dnull);
      asserteq(m, MATH_NOERR);
      m = math_perrorf(MATH_INTERN, dnull);
      asserteq(m, MATH_NOERR);
      m = math_perrorf(MATH_DIMENSION, dnull);
      asserteq(m, MATH_NOERR);
      m = math_perrorf(MATH_DIVZ, dnull);
      asserteq(m, MATH_NOERR);
      m = math_perrorf(MATH_OOB, dnull);
      asserteq(m, MATH_NOERR);
    });
  });
  subdesc(math_errorstring, {
    it("Rejects invalid arguments",{
      MATH_ERROR m = math_errorstring(0, NULL, 1);
      asserteq(m, MATH_INVARG);
      m = math_errorstring(0, NULL, 0);
      asserteq(m, MATH_INVARG);
      char *test = "Hi";
      m = math_errorstring(0, test, 0);
      asserteq(m, MATH_INVARG);
      m = math_errorstring(0, test, sizeof(test));
      assertneq(m, MATH_INVARG);
    });

    it("Rejects buffers that are too short", {
      char *s = malloc(1);
      defer(free(s));
      MATH_ERROR m = math_errorstring(0, s, sizeof(s));
      asserteq(m, MATH_SHRTBUF);
    });

    it("Correctly assigns strings", {
      char s[32];
      MATH_ERROR m;
      m = math_errorstring(MATH_NOERR, s, sizeof(s));
      asserteq(m, MATH_NOERR);
      asserteq("Math: No Error", s);
      m = math_errorstring(MATH_SHRTBUF, s, sizeof(s));
      asserteq(m, MATH_NOERR);
      asserteq("Math: Short Buffer", s);
      m = math_errorstring(MATH_INVARG, s, sizeof(s));
      asserteq(m, MATH_NOERR);
      asserteq("Math: Invalid Argument", s);
      m = math_errorstring(MATH_INTERN, s, sizeof(s));
      asserteq(m, MATH_NOERR);
      asserteq("Math: Internal Function Error", s);
      m = math_errorstring(MATH_OOB, s, sizeof(s));
      asserteq(m, MATH_NOERR);
      asserteq("Math: Out of Bounds", s);
      m = math_errorstring(MATH_DIMENSION, s, sizeof(s));
      asserteq(m, MATH_NOERR);
      asserteq("Math: Dimension Mismatch", s);
      m = math_errorstring(MATH_DIVZ, s, sizeof(s));
      asserteq(m, MATH_NOERR);
      asserteq("Math: Division by Zero", s);
    });

    it("Correctly throws OOB", {
      char s[32];
      MATH_ERROR m = math_errorstring(MATH_OOB + 1, s, sizeof(s));
      asserteq(m, MATH_OOB);
      m = math_errorstring(MATH_OOB + 2, s, sizeof(s));
      asserteq(m, MATH_OOB);
    });
  });
})

snow_main();
