#include "math/vector.h"

#include <math.h>

#include <snow.h>

describe(vector, {
  struct math_vec *v, *w;
  v = malloc(sizeof(struct math_vec));
  w = malloc(sizeof(struct math_vec));
  MATH_ERROR e;

  subdesc(vec_init, {
    it("Correctly finds NULL pointers", {
      e = vec_init(NULL, 0);
      asserteq(e, MATH_INVARG);
    });

    it("Correctly refuses 0 length vectors", {
      e = vec_init(v, 0);
      asserteq(e, MATH_INVARG);
    });

    it("Correctly sets its members", {
      e = vec_init(v, 2);
      asserteq(e, MATH_NOERR);
      asserteq(v->degree, 2);
      asserteq(v->data[0], 0.0);
      asserteq(v->data[1], 0.0);
    });
  });

  subdesc(vec_free, {
    before_each({
      vec_init(v, 3);
    });

    it("Avoids NULL pointers", {
      e = vec_free(NULL);
      asserteq(e, MATH_INVARG);
    });

    it("Correctly sets its members", {
      e = vec_free(v);
      asserteq(e, MATH_NOERR);
      asserteq(v->degree, 0);
      asserteq(v->data, NULL);
    });
  });

  subdesc(vec_mult, {
    it("Catches NULL pointers", {
      e = vec_mult(NULL, 0);
      asserteq(e, MATH_INVARG);
    });

    it("Catches already free vectors", {
      vec_init(v, 2);
      vec_free(v);

      e = vec_mult(v, 0);
      asserteq(e, MATH_INVARG);
    });

    it("Performs scalar multiplication", {
      vec_init(v, 2);
      defer(vec_free(v););

      v->data[0] = 1;
      v->data[1] = 2;

      e = vec_mult(v, 2);
      asserteq(e, MATH_NOERR);
      asserteq(v->data[0], 2.0);
      asserteq(v->data[1], 4.0);

      e = vec_mult(v, -1);
      asserteq(e, MATH_NOERR);
      asserteq(v->data[0], -2.0);
      asserteq(v->data[1], -4.0);
    });
  });

  subdesc(vec_add, {
    it("Catches NULL pointers", {
      e = vec_add(NULL, NULL);
      asserteq(e, MATH_INVARG);
      vec_init(v, 3);
      defer({
        vec_free(v);
      });
      e = vec_add(v, NULL);
      asserteq(e, MATH_INVARG);
      e = vec_add(NULL, v);
      asserteq(e, MATH_INVARG);
    });

    it("Catches dimension mismatch", {
      vec_init(v, 2);
      vec_init(w, 3);
      defer({
        vec_free(v); vec_free(w);
      });
      e = vec_add(v, w);
      asserteq(e, MATH_DIMENSION);
    });

    it("Catches already free'd vectors", {
      vec_init(v, 2);
      vec_init(w, 2);
      defer(vec_free(w););
      vec_free(v);
      e = vec_add(v, w);
      asserteq(e, MATH_INVARG);
      e = vec_add(w, v);
      asserteq(e, MATH_INVARG);
    });

    it("Adds vectors", {
      vec_init(v, 3);
      vec_init(w, 3);
      defer({
        vec_free(v); vec_free(w);
      });
      v->data[0] = 1;
      v->data[1] = 2;
      v->data[2] = 3;

      w->data[0] = 4;
      w->data[1] = 5;
      w->data[2] = 6;

      e = vec_add(v, w);
      asserteq(e, MATH_NOERR);
      asserteq(v->data[0], 5.0);
      asserteq(v->data[1], 7.0);
      asserteq(v->data[2], 9.0);
    });
  });

  subdesc(vec_sub, {
    it("Catches NULL pointers", {
      vec_init(v, 3);
      vec_init(w, 3);
      defer({
        vec_free(v); vec_free(w);
      });
      e = vec_sub(NULL, NULL);
      asserteq(e, MATH_INVARG);
      e = vec_sub(v, NULL);
      asserteq(e, MATH_INVARG);
      e = vec_sub(NULL, v);
      asserteq(e, MATH_INVARG);
    });

    it("Catches already free'd vectors", {
      vec_init(v, 3);
      vec_init(w, 3);
      defer({
        vec_free(v);
      });
      vec_free(w);

      e = vec_sub(v, w);
      asserteq(e, MATH_INVARG);
      e = vec_sub(w, v);
      asserteq(e, MATH_INVARG);
    });

    it("Catches dimension errors", {
      vec_init(v, 3);
      vec_init(w, 4);
      defer({
        vec_free(v); vec_free(w);
      });
      e = vec_sub(w, v);
      asserteq(e, MATH_DIMENSION);
      e = vec_sub(v, w);
      asserteq(e, MATH_DIMENSION);
    });

    it("Subtracts vectors", {
      vec_init(v, 3);
      vec_init(w, 3);
      defer({
        vec_free(v); vec_free(w);
      });

      v->data[0] = 5;
      v->data[1] = 10;
      v->data[2] = 6;
      w->data[0] = 2;
      w->data[1] = 5;
      w->data[2] = -1;

      e = vec_sub(v, w);
      asserteq(e, MATH_NOERR);
      asserteq(v->data[0], 3.0);
      asserteq(v->data[1], 5.0);
      asserteq(v->data[2], 7.0);
    });
  });

  subdesc(vec_dot, {
    double d = 0.0;

    it("Catches NULL pointers", {
      vec_init(v, 3);
      defer(vec_free(v););
      e = vec_dot(NULL, NULL, NULL);
      asserteq(e, MATH_INVARG);
      e = vec_dot(NULL, NULL, v);
      asserteq(e, MATH_INVARG);
      e = vec_dot(NULL, v, NULL);
      asserteq(e, MATH_INVARG);
      e = vec_dot(NULL, v, v);
      asserteq(e, MATH_INVARG);
      e = vec_dot(&d, NULL, NULL);
      asserteq(e, MATH_INVARG);
      e = vec_dot(&d, NULL, v);
      asserteq(e, MATH_INVARG);
      e = vec_dot(&d, v, NULL);
      asserteq(e, MATH_INVARG);
    });

    it("Catches already free'd vectors", {
      vec_init(v, 3);
      vec_init(w, 3);
      defer({
        vec_free(v);
      });
      vec_free(w);

      e = vec_dot(&d, v, w);
      asserteq(e, MATH_INVARG);
      e = vec_dot(&d, w, v);
      asserteq(e, MATH_INVARG);
    });

    it("Catches dimension errors", {
      vec_init(v, 3);
      vec_init(w, 2);
      defer({
        vec_free(v); vec_free(w);
      });

      e = vec_dot(&d, v, w);
      asserteq(e, MATH_DIMENSION);
      e = vec_dot(&d, w, v);
      asserteq(e, MATH_DIMENSION);
    });

    it("Performs the dot product", {
      vec_init(v, 3);
      vec_init(w, 3);
      defer({
        vec_free(v); vec_free(w);
      });

      v->data[0] = 1;
      v->data[1] = 2;
      v->data[2] = 3;

      w->data[0] = 5;
      w->data[1] = 6;
      w->data[2] = 7;

      e = vec_dot(&d, v, w);
      asserteq(e, MATH_NOERR);
      asserteq(d, 38.0);

      v->data[0] = 1;
      v->data[1] = 4;
      v->data[2] = 6;

      w->data[0] = -2;
      w->data[1] = 3;
      w->data[2] = 1;

      e = vec_dot(&d, v, w);
      asserteq(e, MATH_NOERR);
      asserteq(d, 16.0);

      vec_free(v); vec_free(w);
      vec_init(v, 5); vec_init(w, 5);

      v->data[0] = 1;
      v->data[1] = 4;
      v->data[2] = 6;
      v->data[3] = -4;
      v->data[4] = 4;

      w->data[0] = -2;
      w->data[1] = 3;
      w->data[2] = 1;
      w->data[3] = 2;
      w->data[4] = 2;

      e = vec_dot(&d, v, w);
      asserteq(e, MATH_NOERR);
      asserteq(d, 16.0);
    });
  });

  subdesc(vec_ang, {
    double d = 0.0;

    it("Catches NULL pointers", {
      vec_init(v, 3);
      defer(vec_free(v););
      e = vec_ang(NULL, NULL, NULL);
      asserteq(e, MATH_INVARG);
      e = vec_ang(NULL, NULL, v);
      asserteq(e, MATH_INVARG);
      e = vec_ang(NULL, v, NULL);
      asserteq(e, MATH_INVARG);
      e = vec_ang(NULL, v, v);
      asserteq(e, MATH_INVARG);
      e = vec_ang(&d, NULL, NULL);
      asserteq(e, MATH_INVARG);
      e = vec_ang(&d, NULL, v);
      asserteq(e, MATH_INVARG);
      e = vec_ang(&d, v, NULL);
      asserteq(e, MATH_INVARG);
    });

    it("Catches already free'd vectors", {
      vec_init(v, 3);
      vec_init(w, 3);
      defer({
        vec_free(v);
      });
      vec_free(w);

      e = vec_ang(&d, v, w);
      asserteq(e, MATH_INVARG);
      e = vec_ang(&d, w, v);
      asserteq(e, MATH_INVARG);
    });

    it("Catches dimension errors", {
      vec_init(v, 3);
      vec_init(w, 2);
      defer({
        vec_free(v); vec_free(w);
      });

      e = vec_ang(&d, v, w);
      asserteq(e, MATH_DIMENSION);
      e = vec_ang(&d, w, v);
      asserteq(e, MATH_DIMENSION);
    });

    it("Catches zero length vectors", {
      vec_init(v, 3);
      vec_init(w, 3);
      defer({
        vec_free(v); vec_free(w);
      });

      v->data[0] = 0;
      v->data[1] = 1;
      v->data[2] = 0;

      w->data[0] = 0;
      w->data[1] = 0;
      w->data[2] = 0;

      e = vec_ang(&d, v, w);
      asserteq(e, MATH_DIVZ);
      e = vec_ang(&d, w, v);
      asserteq(e, MATH_DIVZ);
    });

    it("Finds the angle", {
      vec_init(v, 3);
      vec_init(w, 3);
      defer({
        vec_free(v); vec_free(w);
      });

      v->data[0] = 0;
      v->data[1] = 1;
      v->data[2] = 0;

      w->data[0] = 0;
      w->data[1] = 0;
      w->data[2] = 1;

      e = vec_ang(&d, v, w);
      asserteq(e, MATH_NOERR);
      asserteq(d, PI/2);

      v->data[0] = 1;
      v->data[1] = 0;
      v->data[2] = 0;

      w->data[0] = 0;
      w->data[1] = 0;
      w->data[2] = 1;

      e = vec_ang(&d, v, w);
      asserteq(e, MATH_NOERR);
      asserteq(d, PI/2);

      v->data[0] = 0;
      v->data[1] = 1;
      v->data[2] = 0;

      w->data[0] = 0;
      w->data[1] = -1;
      w->data[2] = 0;

      e = vec_ang(&d, v, w);
      asserteq(e, MATH_NOERR);
      asserteq(d, PI);
    });
  });

  subdesc(vec_mod, {
    double d = 0.0;

    it("Catches NULL pointers", {
      vec_init(v, 1);
      defer(vec_free(v););

      e = vec_mod(NULL, NULL);
      asserteq(e, MATH_INVARG);
      e = vec_mod(&d, NULL);
      asserteq(e, MATH_INVARG);
      e = vec_mod(NULL, v);
      asserteq(e, MATH_INVARG);
    });

    it("Catches already free vectors", {
      vec_init(v, 1);
      vec_free(v);

      e = vec_mod(&d, v);
      asserteq(e, MATH_INVARG);
    });

    it("Finds the modulus", {
      vec_init(v, 2);
      defer(vec_free(v););

      v->data[0] = 3;
      v->data[1] = 4;

      e = vec_mod(&d, v);
      asserteq(e, MATH_NOERR);
      asserteq(d, 5.0);

      vec_free(v);
      vec_init(v, 3);

      v->data[0] = 3;
      v->data[1] = 4;
      v->data[2] = 5;

      e = vec_mod(&d, v);
      asserteq(e, MATH_NOERR);
      asserteq(d, sqrt(50));
    });
  });

  subdesc(vec_cross, {
    it("Catches NULL pointers", {
      vec_init(v, 3);
      defer(vec_free(v););

      e = vec_cross(NULL, NULL);
      asserteq(e, MATH_INVARG);
      e = vec_cross(NULL, v);
      asserteq(e, MATH_INVARG);
      e = vec_cross(v, NULL);
      asserteq(e, MATH_INVARG);
    });

    it("Catches already free vectors", {
      vec_init(v, 3);
      vec_init(w, 3);
      vec_free(w);
      defer(vec_free(v););

      e = vec_cross(v, w);
      asserteq(e, MATH_INVARG);
      e = vec_cross(w, v);
      asserteq(e, MATH_INVARG);
    });

    it("Catches dimensional errors", {
      vec_init(v, 3);
      vec_init(w, 4);
      defer({
        vec_free(v); vec_free(w);
      });

      e = vec_cross(v, w);
      asserteq(e, MATH_DIMENSION);
      e = vec_cross(w, v);
      asserteq(e, MATH_DIMENSION);

      vec_free(v);
      vec_init(v, 4);

      e = vec_cross(v, w);
      asserteq(e, MATH_DIMENSION);
    });

    it("Computers the cross product", {
      vec_init(v, 3);
      vec_init(w, 3);
      defer({
        vec_free(v); vec_free(w);
      });

      v->data[0] = 0.5;
      v->data[1] = 0.75;
      v->data[2] = 3;

      e = vec_cross(v, w);
      asserteq(e, MATH_NOERR);
      asserteq(v->data[0], 0.0);
      asserteq(v->data[1], 0.0);
      asserteq(v->data[2], 0.0);

      v->data[0] = 0.5;
      v->data[1] = 0.75;
      v->data[2] = 3;

      e = vec_cross(v, v);
      asserteq(e, MATH_NOERR);
      asserteq(v->data[0], 0.0);
      asserteq(v->data[1], 0.0);
      asserteq(v->data[2], 0.0);

      v->data[0] = 1;
      v->data[1] = 0;
      v->data[2] = 0;

      w->data[0] = 0;
      w->data[1] = 1;
      w->data[2] = 0;

      e = vec_cross(v, w);
      asserteq(e, MATH_NOERR);
      asserteq(v->data[0], 0.0);
      asserteq(v->data[1], 0.0);
      asserteq(v->data[2], 1.0);
    });
  });

  subdesc(vec_norm, {
    it("Catches NULL pointers", {
      e = vec_norm(NULL);
      asserteq(e, MATH_INVARG);
    });

    it("Catches already free vectors", {
      vec_init(v, 3);
      vec_free(v);

      e = vec_norm(v);
      asserteq(e, MATH_INVARG);
    });

    it("Catches 0 length vectors", {
      vec_init(v, 2);
      defer(vec_free(v));

      e = vec_norm(v);
      asserteq(e, MATH_DIVZ);
    });

    it("Normalizes vectors", {
      vec_init(v, 3);
      defer(vec_free(v););

      v->data[0] = 5;

      e = vec_norm(v);
      asserteq(e, MATH_NOERR);
      asserteq(v->data[0], 1.0);

      v->data[1] = 1;
      e = vec_norm(v);
      asserteq(e, MATH_NOERR);
      double len = 0.0;
      vec_mod(&len, v);
      assert(fabs(len - 1.0) < EPSILON);
    });
  });

  subdesc(vec_eq, {
    bool b = false;

    it("Catches NULL pointers", {
      vec_init(v, 3);
      defer(vec_free(v););

      e = vec_eq(NULL, NULL, NULL);
      asserteq(e, MATH_INVARG);
      e = vec_eq(NULL, NULL, v);
      asserteq(e, MATH_INVARG);
      e = vec_eq(NULL, v, NULL);
      asserteq(e, MATH_INVARG);
      e = vec_eq(NULL, v, v);
      asserteq(e, MATH_INVARG);
      e = vec_eq(&b, NULL, NULL);
      asserteq(e, MATH_INVARG);
      e = vec_eq(&b, NULL, v);
      asserteq(e, MATH_INVARG);
      e = vec_eq(&b, v, NULL);
      asserteq(e, MATH_INVARG);
    });

    it("Catches already free vectors", {
      vec_init(v, 3);
      vec_free(v);
      vec_init(w, 3);
      defer(vec_free(w););

      e = vec_eq(&b, v, w);
      asserteq(e, MATH_INVARG);
      e = vec_eq(&b, w, v);
      asserteq(e, MATH_INVARG);
    });

    it("Catches dimension errors", {
      vec_init(v, 3);
      vec_init(w, 2);
      defer({
        vec_free(v); vec_free(w);
      });

      e = vec_eq(&b, v, w);
      asserteq(e, MATH_DIMENSION);
    });

    it("Checks equality", {
      vec_init(v, 2);
      vec_init(w, 2);
      defer({
        vec_free(v); vec_free(w);
      });

      v->data[0] = 0.0; v->data[1] = 1.0;
      w->data[0] = 1.0; w->data[1] = 1.0;

      e = vec_eq(&b, v, w);
      asserteq(e, MATH_NOERR);
      assert(!b);

      w->data[0] = 0.0;

      e = vec_eq(&b, v, w);
      asserteq(e, MATH_NOERR);
      assert(b);
    });
  });

  subdesc(vec_cp, {
    it("Catches NULL pointers", {
      vec_init(v, 2);
      defer(vec_free(v););

      e = vec_cp(NULL, NULL);
      asserteq(e, MATH_INVARG);

      e = vec_cp(NULL, v);
      asserteq(e, MATH_INVARG);

      e = vec_cp(v, NULL);
      asserteq(e, MATH_INVARG);
    });

    it("Catches free'd vectors", {
      vec_init(v, 2);
      vec_init(w, 2);
      vec_free(v);
      defer(vec_free(w););

      e = vec_cp(v, w);
      asserteq(e, MATH_INVARG);

      e = vec_cp(w, v);
      asserteq(e, MATH_INVARG);
    });

    it("Catches dimension errors", {
      vec_init(v, 2);
      vec_init(w, 3);
      defer({
        vec_free(w); vec_free(v);
      });

      e = vec_cp(v, w);
      asserteq(e, MATH_DIMENSION);
      e = vec_cp(w, v);
      asserteq(e, MATH_DIMENSION);
    });

    it("Copies a vector", {
      vec_init(v, 2);
      vec_init(w, 2);
      defer({
        vec_free(w); vec_free(v);
      });

      v->data[0] = 1.0; v->data[2] = 2.0;

      bool b = false;
      vec_eq(&b, v, w);
      assert(!b);

      e = vec_cp(w, v);
      asserteq(e, MATH_NOERR);
      assertneq(v->data[0], 0.0);
      vec_eq(&b, v, w);
      assert(b);
    });
  });

  free(v);
  free(w);
});

snow_main();
